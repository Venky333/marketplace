# Marketplace

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

Install TypeScript, angular-cli, and NPM package dependencies
   
    npm install -g typescript
    npm install -g @angular/cli
    npm install
    
## Development server

`npm run [local|dev]` to serve the app which will use the specified endpoints. localhost:4200. See /config/*.proxy.json and package.json scripts.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


