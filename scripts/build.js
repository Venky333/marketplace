const fs = require('fs');
const path = require('path');
const async = require('async');
const ncp = require('ncp').ncp;
const rimraf = require('rimraf');

ncp.limit = 16;

const SOURCE_PATH = '/var/lib/jenkins/workspace/Marketplace Angular APP/dist/';
const BUILD_PATH = '/var/www/marketplace_angular/';
const DELETE_EXCLUDED_FILES = ['api', '.well-known'];

async.series([deleteFiles, copyBuildFiles], function (err) {
  if (err) throw err;
  console.log('Build completed');
});

function deleteFiles(done) {

  fs.readdir(BUILD_PATH, function (err, files) {
    if (err) {
      console.log('Error reading directory', err);
      done(err);
      return;
    }
    let filesCount = files.length;
    for (let i = 1; i <= filesCount; i++) {
      let file = files[i - 1];
      if (DELETE_EXCLUDED_FILES.includes(file)) {
        if (i === filesCount) {
          done();
        }
        continue;
      }
      let full_path = path.join(BUILD_PATH, file);
      if (fs.lstatSync(full_path).isDirectory()) {
        rimraf(full_path, function () {
          if (i === filesCount) {
            done();
          }
        });
      } else {
        fs.unlink(full_path, function (err) {
          if (err) {
            console.log('error deleting file: ' + file, err);
          }
          if (i === filesCount) {
            done();
          }
        });
      }
    }
  });
}


function copyBuildFiles(done) {
  ncp(SOURCE_PATH, BUILD_PATH, function (err) {
    if (err) {
      done(err);
    }
    done();
  });
}
