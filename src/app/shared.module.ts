import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {ImageInputComponent} from './components/image-input/image-input.component';
import {BuyerMenubarComponent} from './components/buyer-menu-bar/buyer-menubar.component';
import {BuyerMobileMenuComponent} from './components/buyer-mobile-menu/buyer-mobile-menu.component';
import {SellerMenubarComponent} from './components/seller-menu-bar/seller-menubar.component';
import {SellerMobileMenuComponent} from './components/seller-mobile-menu/seller-mobile-menu.component';
import {ProductSpecialLabelComponent} from './components/product-special-label/product-special-label.component';
import {CategoryMenuBarComponent} from './components/category-menu-bar/category-menu-bar.component';
import { CategoryMobileMenuBarComponent } from './components/category-mobile-menu-bar/category-mobile-menu-bar.component';
import {OrderStateIconComponent} from './components/order-state-icon/order-state-icon.component';
import {OrderStateLabelComponent} from './components/order-state-label/order-state-label.component';
import {OrderDetailsModalComponent} from './components/order-details-modal/order-details-modal.component';
import {LoadingPlaceholderComponent} from './components/loading-placeholder/loading-placeholder.component';
import {PopularProductsComponent} from './components/popular-products/popular-products.component';
import {ProductAccessoriesComponent} from './components/product-accessories/product-accessories.component';
import {RelatedProductsWidgetComponent} from './components/related-products/related-products.component';
import {ProductFullViewComponent} from './components/product-view/full-view/product-full-view.component';
import {ProductCardViewComponent} from './components/product-view/card-view/product-card-view.component';
import {ProductDescriptionComponent} from './components/product-view/product-description/product-description.component';
import {QuantityInputComponent} from './components/quantity-input/quantity-input.component';
import {OrderStatusIconComponent} from './components/order-status-icon/order-status-icon.component';
import {NgxShaveDirective} from './components/ngx-shave/ngx-shave.directive';
import {ProductModalViewComponent} from './components/product-modal-view/product-modal-view.component';
import {FeedbackButtonComponent} from './components/feedback-button';
import {ProductsWidgetComponent} from './components/products-widget/products-widget.component';

// Shared components and directives that other modules should probably import
@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    LazyLoadImageModule,
    NgbDropdownModule,
    NgbTooltipModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ImageInputComponent,
    BuyerMenubarComponent,
    BuyerMobileMenuComponent,
    SellerMenubarComponent,
    SellerMobileMenuComponent,
    OrderStatusIconComponent,
    ProductSpecialLabelComponent,
    CategoryMobileMenuBarComponent,
    CategoryMenuBarComponent,
    OrderStateIconComponent,
    OrderDetailsModalComponent,
    PopularProductsComponent,
    RelatedProductsWidgetComponent,
    ProductAccessoriesComponent,
    OrderStateLabelComponent,
    LoadingPlaceholderComponent,
    ProductFullViewComponent,
    ProductCardViewComponent,
    ProductDescriptionComponent,
    QuantityInputComponent,
    NgxShaveDirective,
    ProductModalViewComponent,
    FeedbackButtonComponent,
    ProductsWidgetComponent
  ],
  exports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LazyLoadImageModule,
    ImageInputComponent,
    PopularProductsComponent,
    ProductAccessoriesComponent,
    RelatedProductsWidgetComponent,
    BuyerMenubarComponent,
    BuyerMobileMenuComponent,
    SellerMenubarComponent,
    SellerMobileMenuComponent,
    ProductSpecialLabelComponent,
    CategoryMenuBarComponent,
    OrderStatusIconComponent,
    OrderStateIconComponent,
    OrderStateLabelComponent,
    OrderDetailsModalComponent,
    LoadingPlaceholderComponent,
    ProductFullViewComponent,
    ProductCardViewComponent,
    ProductDescriptionComponent,
    QuantityInputComponent,
    NgxShaveDirective,
    ProductModalViewComponent,
    FeedbackButtonComponent,
    ProductsWidgetComponent,
  ],
  entryComponents: [
    OrderDetailsModalComponent,
    ProductModalViewComponent
  ]
})
export class SharedModule {
}
