import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from 'app/services/auth';

@Injectable({'providedIn': 'root'})
export class LoggedInGuard implements CanActivate {

  static TAG: 'LoggedInGuard';

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(): boolean {
    const isLoggedIn = this.authService.isLoggedIn();
    if (!isLoggedIn) {
      console.debug(LoggedInGuard.TAG, 'activation denied');
      this.router.navigate(['/']);
    }
    return isLoggedIn;
  }

}
