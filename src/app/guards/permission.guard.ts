import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from 'app/services/auth';

@Injectable({'providedIn': 'root'})
export class PermissionGuard implements CanActivate {

  static TAG: 'PermissionGuard';

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const roles = route.data['permissions'] as Array<string>;
    if (!this.authService.userCan(roles)) {
      console.debug(PermissionGuard.TAG, 'activation denied');
      this.router.navigate(['/']);
    }

    return true;
  }

}
