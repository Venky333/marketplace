import { HttpErrorResponse } from '@angular/common/http';

/**
 * tarket mixin must have this.router: Router instance
 */
export const ErrorRerouteMixin = {

  handleErrorResp(fail: HttpErrorResponse) {
    if (fail.status === 404) {
      (<any>this).router.navigate(['/not-found'], {replaceUrl: true});
    } else {
      (<any>this).router.navigate(['/app-error'], {replaceUrl: true});
      console.error('ErrorReouteMixin', fail);
    }
    return false;
  },

};
