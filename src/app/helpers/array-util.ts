export function groupBy<TSource, TKey>(source: TSource[], getKey: (item: TSource) => TKey): Map<TKey, TSource[]> {
  const groups = new Map<TKey, TSource[]>();

  source.forEach((item) => {
    const key = getKey(item);
    if (!groups.has(key)) {
      groups.set(key, []);
    }
    groups.get(key).push(item);
  });

  return groups;
}

// Allows splicing several values into another array as a block.
// Behavior is the same as Array.prototype.splice except a) the `values` parameter is an array and b) the context array
// is provided as an argument rather than using `this`.
// Returns: An array containing the deleted elements, if any.
export function arraySplice<T>(array: Array<T>, start: number, deleteCount?: number, values?: Array<T>): Array<T> {
  return [].splice.apply(array, [start, deleteCount].concat(<any[]>values || []));
}

// Moves the item in `array` at position `from` to position `to`. All items in between are shifted one position as appropriate.
export function arrayMove(array: Array<any>, from: number, to: number): void {
  if (to !== from) {
    array.splice(to, 0, array.splice(from, 1)[0]);
  }
}

// Removes an item from an array, if present.
// Arguments:
//  array - the array to search.
//  item - the item to remove (must be a primitive or the exact object reference;
// i.e. must be the same object according to the === operator).
// Returns:
//  The item if found; else null.
export function remove<T>(array: Array<T>, item: T): T {
  const index = array.indexOf(item);
  if (index !== -1) {
    return array.splice(index, 1)[0];
  }
  return null;
}

// Removes the first item, if any, that satisfies the testing function.
// Arguments:
//  array - the array to search.
//  callback - a function with the same behavior as the callback argument to Array.prototype.findIndex.
//  context - (optional) an object to use as `this` when executing the callback.
// Returns:
//  The item if found; else null.
export function removeFind<T>(array: Array<T>, callback: (item: T) => boolean, context?: any): T {
  const index = array.findIndex(callback, context);
  if (index !== -1) {
    return array.splice(index, 1)[0];
  }
  return null;
}

export function chunk<T>(array: Array<T>, size: number): Array<T> {
  let i, j, temp;
  const result = [];
  for (i = 0, j = array.length; i < j; i += size) {
    temp = array.slice(i, i + size);
    result.push(temp);
  }
  return result;
}
