import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export function scrollToTop() {
  window.scroll(0, 0);
}

export function getErrorMsg(fail) {
  return (fail.error.message ? fail.error.message : (fail.message ? fail.message : 'Unknown error'));
}

export function isInteger(x): boolean {
  return Number.isInteger(x);
}

export function formatNumber(e, field: string) {
  if (e.keyCode === 9 || e.keyCode === 16) {
    return;
  }
  if (e.keyCode === 8 && (e.target.value === '$' || e.target.value === '')) {
    this[field] = null;
    return;
  }

  const n = Number(e.target.value.replace(/\$|,/g, ''));
  this[field] = Number.isInteger(n) ? n : null;
}

/**
 * @param e DOM Event
 * prevents key if it is not a number or navigation
 */
export function enforceNumber(e) {
  if (!((e.keyCode >= 33 && e.keyCode <= 57)
    || (e.keyCode >= 96 && e.keyCode <= 105) // numpad
    || e.keyCode === 8  // back
    || e.keyCode === 13 // enter
    || (e.keyCode >= 16 && e.keyCode <= 20)
    || e.keyCode === 27 // esc
    || e.keyCode === 9 // tab
  )) {
    e.preventDefault();
  }
}

export function isDecimal(x): boolean {
  return !isNaN(Number(x));
}

export function formatDecimalNumber(e, field: string) {
  if (e.keyCode === 9 || e.keyCode === 16) {
    return;
  }
  if (e.keyCode === 8 && (e.target.value === '$' || e.target.value === '')) {
    this[field] = null;
    return;
  }

  const n = Number(e.target.value.replace(/\$|,/g, ''));
  this[field] = !isNaN(n) ? n : null;
}


/**
 * @param e DOM Event
 * prevents key if it is not a number or navigation
 */
export function enforceDecimalNumber(e) {
  if (!((e.keyCode >= 33 && e.keyCode <= 57)
    || (e.keyCode >= 96 && e.keyCode <= 105) // numpad
    || e.keyCode === 110  // decimal point
    || e.keyCode === 190  // period
    || e.keyCode === 8  // back
    || e.keyCode === 13 // enter
    || (e.keyCode >= 16 && e.keyCode <= 20)
    || e.keyCode === 27 // esc
    || e.keyCode === 9 // tab
  )) {
    e.preventDefault();
  }
}

export class FormHelper {

  static markAsTouched(control: AbstractControl) {
    if (control.hasOwnProperty('controls')) {
      const ctrl = <any>control;
      for (const inner in ctrl.controls) {
        if (ctrl.controls.hasOwnProperty(inner)) {
          FormHelper.markAsTouched(ctrl.controls[inner] as AbstractControl);
        }
      }
    } else {
      (<FormControl>(control)).markAsTouched({onlySelf: true});
    }
  }

  static biggerThan(min: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (isEmptyInputValue(control.value) || isEmptyInputValue(min)) {
        return null;  // don't validate empty values to allow optional controls
      }
      const value = parseFloat(control.value);
      // Controls with NaN values after parsing should be treated as not having a
      // minimum, per the HTML forms spec: https://www.w3.org/TR/html5/forms.html#attr-input-min
      return !isNaN(value) && value <= min ? {'biggerThan': {'biggerThan': min, 'actual': control.value}} : null;
    };
  }


  static number(control: AbstractControl): ValidationErrors {
    if (isEmptyInputValue(control.value)) {
      return null;  // don't validate empty values to allow optional controls
    }

    return isNaN(control.value) ? {'number': true} : null;
  };

}


export function checkDiscountSmallThanPrice(c: AbstractControl) {
  // safety check
  if (!c.value || !c.parent.get('price').value) {
    return null;
  }
  const discount = parseFloat(c.value);
  const priceValue = parseFloat(c.parent.get('price').value);

  return (!isNaN(discount) && !isNaN(priceValue) && discount >= priceValue) ? {discountLargeThanPrice: true} : null;
}


function isEmptyInputValue(value: any): boolean {
  // we don't check for string here so it also works with arrays
  return value == null || value.length === 0;
}


/** @description Capitalizes the first letter of a string. Has no effect if the first charater is not a letter.
 * @param {string} str The string to capitalize.
 * @return {string}
 */
export function capitalize(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function createSlug(name: string) {
  return name.replace(/[\(\)\/\-\&\;\%\$]+/g, ' ').split(' ').filter(f => f).join('-');
}


export function calcOffDiscount(price: number, discount_price: number) {
  return 100 - Math.round((discount_price / price) * 100);
}

const MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
export function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / MS_PER_DAY);
}

const MS_PER_HOUR = 1000 * 60 * 60;

// a and b are javascript Date objects
export function dateDiffInHours(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / MS_PER_HOUR);
}

export function matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
  return (group: FormGroup): { [key: string]: any } => {
    const password = group.controls[passwordKey];
    const confirmPassword = group.controls[confirmPasswordKey];
    let result = null;
    if (password.value !== confirmPassword.value) {
      result = {
        mismatchedPasswords: true
      };
    }
    group.controls[confirmPasswordKey].setErrors(result);
    return result;
  };
}
