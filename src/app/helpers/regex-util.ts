export class RegexUtil {

  static EMAIL_PATTERN = '[^@\\s]+@[^@\\s]+\\.[^@\\s]+';
  static PASSWORD_PATTERN = '^(?=.*[a-zA-Z])(?=.*[\\d])\\S{6,64}$';
  static NON_ZERO_POSITIVE_NUMBERS = /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/;
  static NUMBER_PATTERN = new RegExp(/^\d+$/);

}
