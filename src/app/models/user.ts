export interface User {
  id: string;
  profile_id: string;
  type: number;
  email: string;
  nickname: string;
  public_identity?: string;
  public_picture?: string;
  alias: string;
  cover: string;
  avatar: string;
}
