export enum OrderStatusEnum {
  PENDING = 1,
  PROCESSING = 2,
  COMPLETED = 3,
  CANCELLED = 4,
}
