import {OrderProduct} from './order-product';
import {Order} from './order';

export interface SubOrder {
  id: number;
  order_id: number;
  status: number;
  consignment_number: string;
  shipping_service_id?: number;
  seller_id: number;
  updated_at: Date;
  mainOrder?: Order;
  ordered_unit?: number;
  shipped_unit?: number;
  total?: number;
  state?: number;
  is_completed?: boolean;
  created_at: Date,
  orderProducts: Array<OrderProduct>;
}

