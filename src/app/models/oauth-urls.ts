export interface OauthUrls {
  loginUrl: string;
  logoutUrl: string;
  registerUrl: string;
}