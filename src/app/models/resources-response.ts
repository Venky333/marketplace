export interface ListingResponse<T> {
  data: T[];
  _links?: {
    self: { href: string }
    first?: { href: string }
    last?: { href: string }
    next?: { href: string }
    prev?: { href: string }
  };
  _meta?: { totalCount: number, pageCount: number, currentPage: number, perPage: number };
}
