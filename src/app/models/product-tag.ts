export interface ProductTag {
  id?: number;
  name: string;
}