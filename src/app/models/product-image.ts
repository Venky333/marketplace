export interface ProductImage {
  is_primary: boolean;
  path: string;
  url: string;
  variants?: { 96: string, 128: string, 256: string, 512: string, 768: string };
}
