export interface AuthToken {
  token: string;
  accountsApiToken: string;
  userId: string;
  permissions: string[]
}
