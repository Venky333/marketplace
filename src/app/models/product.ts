import {ProductImage} from './product-image';

export interface Product {
  id: number;
  user_id: number;
  category_id: number;
  minimum_order_quantity: number;
  status: number;
  description: string;
  price: number;
  discount_price?: number;
  model: string;
  brand?: string;
  name: string;
  quantity: number;
  sku: string;
  tags: string[];
  price_on_application: boolean;
  unit_of_measure: string;
  primaryImage: ProductImage;
  images?: Array<ProductImage>;
  link?: string;
  is_special: boolean;
  is_clearance: boolean;
  sold_unit?: number;
  created_at: Date;
  updated_at: Date;
  requires_shipping: boolean;
  weight: number;
  length: number;
  height: number;
  width: number;
  shipping_weight: number;
  shipping_length: number;
  shipping_height: number;
  shipping_width: number;
  discounted_sold_unit?: number;
  labels: Array<any>;
  is_deleted: boolean;
}
