import {Product} from './product';

export interface OrderProduct {
  id: number;
  product_id: number;
  order_id: number;
  sub_order_id: number;
  quantity: number;
  shipped_quantity?: number;
  name: string;
  model?: string;
  price: number;
  tax?: number;
  total: number;
  discount: number;
  shipped_at?: Date;
  product?: Product;
}
