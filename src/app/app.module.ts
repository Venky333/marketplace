import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRootComponent} from './app-root.component';
import {CoreServicesModule} from './core.module';
import {AppRoutes} from './app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    CoreServicesModule,
    AppRoutes
  ],
  declarations: [
    AppRootComponent
  ],
  bootstrap: [AppRootComponent]
})
export class AppModule {
}
