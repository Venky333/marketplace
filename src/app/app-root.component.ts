import {Component, OnInit} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-root',
  template: '<div id="app" class="app"><router-outlet></router-outlet></div>',
  styleUrls: ['./app-root.component.scss']
})
export class AppRootComponent implements OnInit {
  static TAG = 'AppRootComponent';

  constructor(private router: Router,
              private location: Location) {
  }

  ngOnInit() {
    this.router.events.subscribe((nav: any) => {
      if (nav instanceof NavigationStart) {
        const currentUrl = this.location.path().split('?')[0];
        const targetUrl = nav.url.split('?')[0];
        if (currentUrl !== targetUrl || targetUrl.includes('documents')) {
          window.scrollTo(0, 0);
        }
      }
    });
  }
}
