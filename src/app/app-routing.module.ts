import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {
  NgbCarouselModule,
  NgbCollapseModule,
  NgbDropdownModule,
  NgbModalModule,
  NgbTabsetModule,
  NgbTooltipModule,
  NgbTypeaheadModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppErrorPageComponent } from './pages/main/errors/app-error/app-error-page.component';
import { NotFoundPage } from './pages/main/errors/not-found/not-found';
import { ProductDetailsPageComponent } from './pages/main/product/product-details-page.component';
import { SharedModule } from './shared.module';
import { HomePageComponent } from './pages/main/home/home-page.component';
import { MainPageComponent } from './pages/main/main-page.component';
import { AUTH_PAGE_CONFIG, AuthPageConfig } from './pages/auth/config';
import { UserResolver } from './services/user';
import { ListingPageComponent } from './pages/main/listing/listing-page.component';
import { FilterTags } from './pages/main/listing/filter-tags/filter-tags';
import { CheckoutPageComponent } from './pages/main/checkout/checkout-page.component';
import { OrdersPageComponent } from './pages/main/orders/orders-page.component';
import { OrderDetailsTableComponent } from './pages/main/components/order-details-table/order-details-table-component';
import { OrderDetailsPageComponent } from './pages/main/order-details-page/order-details-page.component';
import { BuyNowPageComponent } from './pages/main/buy-now/buy-now-page.component';
import { CheckoutFormComponent } from './pages/main/components/checkout-form/checkout-form.component';
import { SearchFormComponent } from './pages/main/components/search-form/search-form.component';
import { LoggedInGuard } from './guards';
import { GoTopButtonModule } from './components/go-top-button';
import { PaymentFailPageComponent } from './pages/main/payment-fail/controller';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import { AppFooterComponent } from './components/app-footer/app-footer.component';
import { CategoryMenuBarComponent } from './components/category-menu-bar/category-menu-bar.component';
import { CategoryMobileMenuBarComponent } from './components/category-mobile-menu-bar/category-mobile-menu-bar.component';
import { ShoppingCartPageComponent } from './pages/main/shopping-cart/shopping-cart-page.component';
import { MiniShoppingCartComponent } from './pages/main/components/mini-shopping-cart/mini-shopping-cart.component';
import { BuyerMenubarComponent } from './components/buyer-menu-bar/buyer-menubar.component';
import { BuyerMobileMenuComponent } from './components/buyer-mobile-menu/buyer-mobile-menu.component';
import { TermsPageComponent } from './pages/main/terms/terms-page.component';
import { PrivacyPageComponent } from './pages/main/privacy/privacy-page.component';
import { FavoritesPageComponent } from './pages/main/favorites/favorites-page.component';
import { OrderCompletePageComponent } from './pages/main/order-complete-page/order-complete-page.component';
import { OrderInfoFooterComponent } from './pages/main/components/order-info-footer/order-info-footer.component';
import { PermissionGuard } from './guards/permission.guard';
import { AuthService } from './services/auth';
import { ProductDescriptionTabComponent } from './pages/main/product/components/product-description-tab/product-description-tab.component';
import { ProductDetailsTabComponent } from './pages/main/product/components/product-details-tab/product-details-tab.component';

const authPageConfig: AuthPageConfig = {
  loginSuccessRoute: '',
  loginErrorRoute: '/app-error',
  privacyRoute: '/home',
  termsRoute: '/home',
  signUpSuccessRoute: '/home',
  loginRoute: '/auth/login',
  signUpRoute: '/auth/sign-up'
};

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: 'app/pages/auth/module#AuthModule'
  },
  {
    path: '',
    component: MainPageComponent,
    resolve: { user: UserResolver },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            component: HomePageComponent
          },
          {
            path: '',
            component: CategoryMenuBarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            outlet: 'mobile-menu',
            component: CategoryMobileMenuBarComponent
          }
        ]
      },
      {
        path: 'listing',
        children: [
          {
            path: '',
            component: ListingPageComponent
          },
          {
            path: '',
            component: CategoryMenuBarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            outlet: 'mobile-menu',
            component: CategoryMobileMenuBarComponent
          }
        ]
      },
      {
        path: 'terms',
        children: [
          {
            path: '',
            component: TermsPageComponent
          },
          {
            path: '',
            component: CategoryMenuBarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            outlet: 'mobile-menu',
            component: CategoryMobileMenuBarComponent
          }
        ]
      },
      {
        path: 'privacy',
        children: [
          {
            path: '',
            component: PrivacyPageComponent
          },
          {
            path: '',
            component: CategoryMenuBarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            outlet: 'mobile-menu',
            component: CategoryMobileMenuBarComponent
          }
        ]
      },
      {
        path: 'product/:name/:id',
        children: [
          {
            path: '',
            component: ProductDetailsPageComponent
          },
          {
            path: '',
            component: CategoryMenuBarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            outlet: 'mobile-menu',
            component: CategoryMobileMenuBarComponent
          }
        ]
      },
      {
        path: 'favorites',
        children: [
          {
            path: '',
            component: FavoritesPageComponent
          },
          {
            path: '',
            component: BuyerMenubarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            component: BuyerMobileMenuComponent,
            outlet: 'mobile-menu'
          }
        ],
        canActivate: [LoggedInGuard]
      },
      {
        path: 'cart',
        children: [
          {
            path: '',
            component: ShoppingCartPageComponent
          },
          {
            path: '',
            component: BuyerMenubarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            component: BuyerMobileMenuComponent,
            outlet: 'mobile-menu'
          }
        ]
      },
      {
        path: 'cart/checkout',
        children: [
          {
            path: '',
            component: CheckoutPageComponent
          },
          {
            path: '',
            component: BuyerMenubarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            component: BuyerMobileMenuComponent,
            outlet: 'mobile-menu'
          }
        ]
      },
      {
        path: 'buy-now/:id',
        children: [
          {
            path: '',
            component: BuyNowPageComponent
          },
          {
            path: '',
            component: BuyerMenubarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            component: BuyerMobileMenuComponent,
            outlet: 'mobile-menu'
          }
        ]
      },
      {
        path: 'orders',
        children: [
          {
            path: '',
            component: OrdersPageComponent
          },
          {
            path: '',
            component: BuyerMenubarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            component: BuyerMobileMenuComponent,
            outlet: 'mobile-menu'
          }
        ],
        canActivate: [LoggedInGuard]
      },
      {
        path: 'orders/complete/:order_id',
        children: [
          {
            path: '',
            component: OrderCompletePageComponent
          },
          {
            path: '',
            component: BuyerMenubarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            component: BuyerMobileMenuComponent,
            outlet: 'mobile-menu'
          }
        ]
      },
      {
        path: 'orders/:order_id',
        children: [
          {
            path: '',
            component: OrderDetailsPageComponent
          },
          {
            path: '',
            component: BuyerMenubarComponent,
            outlet: 'menubar'
          },
          {
            path: '',
            component: BuyerMobileMenuComponent,
            outlet: 'mobile-menu'
          }
        ],
        canActivate: [LoggedInGuard]
      },
      {
        path: 'payment-fail',
        component: PaymentFailPageComponent
      },
      // Seller panel
      {
        path: 'seller',
        loadChildren: 'app/pages/seller/seller.module#SellerModule',
        data: {
          permissions: [AuthService.MANAGE_PRODUCTS]
        },
        canActivate: [LoggedInGuard, PermissionGuard]
      },
      // Settings
      {
        path: 'settings',
        loadChildren: 'app/pages/settings/settings.module#SettingsModule',
        canActivate: [LoggedInGuard]
      },
      // Errors
      {
        path: 'not-found',
        component: NotFoundPage
      },
      {
        path: 'app-error',
        component: AppErrorPageComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
];

@NgModule({
  imports: [
    SharedModule,
    ReactiveFormsModule,
    NgbModalModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbTabsetModule,
    NgSelectModule,
    GoTopButtonModule,
    NgbTypeaheadModule,
    NgbCollapseModule,
    NgbDropdownModule,
    NgbCollapseModule,
    NgbTooltipModule,
    NgbCarouselModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule],
  providers: [{ provide: AUTH_PAGE_CONFIG, useValue: authPageConfig }],
  declarations: [
    MainPageComponent,
    AppHeaderComponent,
    AppFooterComponent,
    MiniShoppingCartComponent,
    SearchFormComponent,
    HomePageComponent,
    FavoritesPageComponent,
    OrderDetailsTableComponent,
    OrderCompletePageComponent,
    ListingPageComponent,
    FilterTags,
    CheckoutPageComponent,
    ShoppingCartPageComponent,
    PaymentFailPageComponent,
    CheckoutFormComponent,
    OrdersPageComponent,
    OrderDetailsPageComponent,
    BuyNowPageComponent,
    ProductDetailsPageComponent,
    ProductDescriptionTabComponent,
    ProductDetailsTabComponent,
    OrderInfoFooterComponent,
    TermsPageComponent,
    PrivacyPageComponent,
    NotFoundPage,
    AppErrorPageComponent
  ],
  entryComponents: [SearchFormComponent]
})
export class AppRoutes {}
