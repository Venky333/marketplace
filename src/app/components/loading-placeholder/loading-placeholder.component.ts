import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-loading-placeholder',
  templateUrl: './loading-placeholder.component.html',
  styleUrls: ['./loading-placeholder.component.scss']
})

export class LoadingPlaceholderComponent {

  @Input() data: any;
  @Input() error: boolean;
  @Input() showBorder: boolean;

}
