import {Component} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {finalize, map, tap} from 'rxjs/operators';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import {SubOrder} from 'app/models/sub-order';
import {OrderProduct} from 'app/models/order-product';
import {OrderStatusEnum} from 'app/models/order-status-enum';
import {dateDiffInDays, dateDiffInHours} from 'app/helpers/utils';
import {ShippingServiceModel} from 'app/models/shipping-service-model';
import {RegionService} from 'app/services/region.service';
import {SellerOrderService} from 'app/pages/seller/order/seller-order.service';
import {ToastrService} from 'ngx-toastr';
import {AccountService} from '../../services/account/account.service';
import {ShippingService} from '../../services/shipping.service';

@Component({
  selector: 'app-seller-details-modal',
  templateUrl: './order-details-modal.component.html',
  styleUrls: ['./order-details-modal.component.scss'],
})
export class OrderDetailsModalComponent {
  order: SubOrder;
  orderDetail$: Observable<SubOrder>;
  shippingCountry: string;
  form: FormGroup;
  STATUS_COMPLETED = OrderStatusEnum.COMPLETED;
  orderedAgo: string;
  orderedAgoClass;
  total: number;
  totalProducts: number;
  totalItems: number;
  totalShippedItems: number;
  shippingServices: ShippingServiceModel[];
  updating = false;
  recentOrders$: Observable<SubOrder[]>;
  hasMoreOrder = false;
  loading: boolean;
  buyerName$: Observable<string>;
  private orderProducts;

  constructor(private orderService: SellerOrderService,
              private regionService: RegionService,
              public activeModal: NgbActiveModal,
              public accountService: AccountService,
              private shippingService: ShippingService,
              private toastr: ToastrService,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      shipping_service_id: null,
      consignment_number: null,
      items: this.fb.array([])
    });
  }

  setOrder(order: SubOrder): void {
    this.order = order;
    this.fetchOrderDetail();
    this.form.patchValue({
      consignment_number: order.consignment_number,
      shipping_service_id: order.shipping_service_id
    });
    this.calcOrderedAgo();
    this.buyerName$ = this.accountService.fetchProfile(this.order.mainOrder.user_id)
      .pipe(
        map(profile => profile.public_identity)
      )
  }


  saveOrder() {
    this.setOptionalValidators();
    if (this.form.invalid) {
      return false;
    }
    this.doUpdate(this.order.status).subscribe();
  }

  completeOrder() {
    this.setMandatoryValidators();
    if (this.form.invalid) {
      return false;
    }
    this.doUpdate(OrderStatusEnum.COMPLETED).subscribe(() => {
      this.activeModal.close();
      this.toastr.success('Order completed');
    });
  }

  orderProductTrack(index, item) {
    if (!item) {
      return null;
    }
    return item.id;
  }

  private fetchOrderDetail() {
    this.loading = true;
    this.orderDetail$ = this.orderService.fetch(this.order.id)
      .pipe(
        finalize(() => this.loading = false),
        tap((order: SubOrder) => {
          this.form.setControl('items', new FormArray([]));
          const control = this.form.get('items') as FormArray;
          this.orderProducts = order.orderProducts;
          order.orderProducts.forEach(item => control.push(this.createItem(item)));
          if (order.status === this.STATUS_COMPLETED) {
            this.form.disable({onlySelf: false});
          } else {
            this.form.enable({onlySelf: false});
          }
          this.setOptionalValidators();
          this.calcTotals();
          this.fetchBuyerRecentOrders(this.order.mainOrder.user_id);
        })
      );
    this.regionService.getCountry(this.order.mainOrder.shipping_country_id).subscribe(c => this.shippingCountry = c.name);
    this.shippingService.fetch().subscribe((data: any) => {
      this.shippingServices = data.shipping_services;
    });
  }

  private calcOrderedAgo() {
    const now = new Date();
    const orderedDate = new Date(this.order.created_at);
    let diff = dateDiffInDays(orderedDate, now);
    this.orderedAgoClass = 'normal';
    if (diff === 0) {
      diff = dateDiffInHours(orderedDate, now);
      this.orderedAgo = `Ordered ${diff} hour${diff > 1 ? 's' : ''} ago`;
    } else {
      this.orderedAgo = `Ordered ${diff} day${diff > 1 ? 's' : ''} ago`;
      if (diff > 1 && diff < 2) {
        this.orderedAgoClass = 'warning';
      } else if (diff > 2) {
        this.orderedAgoClass = 'danger';
      }
    }
  }

  private calcTotals() {
    const result = this.orderService.calcTotals(this.orderProducts);
    this.totalItems = result.itemsCount;
    this.total = result.total;
    this.totalProducts = this.orderProducts.length;
    this.totalShippedItems = this.orderProducts.reduce((acc, curr) => acc + curr.shipped_quantity, 0);
  }

  private createItem(item: OrderProduct) {
    return this.fb.group({
      shipped_quantity: [item.shipped_quantity],
      id: item.id
    });
  }

  private setMandatoryValidators() {
    ['shipping_service_id', 'consignment_number'].forEach((field) => {
      this.form.get(field).setValidators([Validators.required]);
      this.form.get(field).updateValueAndValidity();
    });
    const itemsControl = this.form.get('items') as FormArray;
    itemsControl.controls.forEach((control, index) => {
      const item = this.orderProducts[index];
      control.get('shipped_quantity').setValidators([Validators.required, Validators.max(item.quantity), Validators.min(item.quantity)]);
      control.get('shipped_quantity').updateValueAndValidity();
    });

  }

  private setOptionalValidators() {
    ['shipping_service_id', 'consignment_number'].forEach((field) => {
      this.form.get(field).setValidators(null);
      this.form.get(field).updateValueAndValidity();
    });
    const itemsControl = this.form.get('items') as FormArray;
    itemsControl.controls.forEach((control, index) => {
      const item = this.orderProducts[index];
      control.get('shipped_quantity').setValidators([Validators.min(0), Validators.max(item.quantity)]);
      control.get('shipped_quantity').updateValueAndValidity();
    });
  }

  private doUpdate(status) {
    this.updating = true;
    const orderProducts = this.orderProducts.map(orderProduct => ({
      id: orderProduct.id,
      shipped_quantity: orderProduct.shipped_quantity
    }));
    const formValue = this.form.value;
    const data = {
      status,
      shipping_service_id: formValue.shipping_service_id,
      consignment_number: formValue.consignment_number
    };

    formValue.items.forEach((orderItem, index) => {
      orderProducts[index].shipped_quantity = orderItem.shipped_quantity;
    });
    data['orderProducts'] = orderProducts;
    return this.orderService.update(this.order.id, data)
      .pipe(tap(() => this.updating = false));
  }

  private fetchBuyerRecentOrders(buyer_id) {
    this.recentOrders$ = this.orderService.query({
      sort: '-created_at',
      buyer_id,
      'per-page': 5,
      expand: 'mainOrder,orderProducts,orderProducts.product'
    })
      .pipe(
        map((response: any) => {
          this.hasMoreOrder = response.meta.last_page > 1;
          return response.subOrders;
        })
      )
  }
}
