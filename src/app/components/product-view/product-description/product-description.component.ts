import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'app-product-description',
  template: `
    <div [innerHtml]="description"></div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDescriptionComponent implements OnChanges {

  description: SafeHtml;

  @Input() product;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('product')) {
      this.description = this.sanitizer.bypassSecurityTrustHtml(this.product.description);
    }
  }

}
