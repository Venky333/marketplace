import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

import {ShoppingCartService} from 'app/services/shopping-cart';
import {calcOffDiscount} from 'app/helpers/utils';
import {ProductService} from 'app/services/product';
import {AuthService} from 'app/services/auth';
import {FavoritesService} from 'app/services/favorites.service';
import {AccountService} from 'app/services/account/account.service';
import {Product, User} from 'app/models';
import {TrackService} from 'app/services/track.service';
import {FeedbackButtonComponent} from '../../feedback-button';

@Component({
  selector: 'app-product-full-view',
  templateUrl: './product-full-view.component.html',
  styleUrls: ['./product-full-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductFullViewComponent implements OnInit, OnChanges {

  @Input() product: Product;
  @Input() showItemActions = true;

  loggedIn = false;
  primaryImage;
  quantity = 1;
  STATUS_ACTIVE = ProductService.PRODUCT_STATUS_PUBLISHED;
  calcOffDiscount = calcOffDiscount;
  addingFavorite = false;
  addedFavorite = false;
  sellerInfo: User;
  addCartState = FeedbackButtonComponent.READY;
  addCartDisabled = false;

  constructor(private shoppingCartService: ShoppingCartService,
              private favoriteService: FavoritesService,
              private toastr: ToastrService,
              private accountService: AccountService,
              private cd: ChangeDetectorRef,
              private trackService: TrackService,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.resetProduct();
    this.loggedIn = this.authService.isLoggedIn();
    this.favoriteService.find(this.product.id)
      .subscribe(() => {
        this.addedFavorite = true;
        this.cd.markForCheck();
      }, () => {
      });
    this.trackService.productView(this.product.id).subscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('product')) {
      this.resetProduct();
    }
  }

  setPrimaryImage(i) {
    this.primaryImage = this.product.images[i];
    this.cd.markForCheck();
  }

  updateQuantity(value) {
    this.quantity = value;
  }

  addToCart() {
    this.addCartState = FeedbackButtonComponent.WAITING;
    this.addCartDisabled = true;
    this.shoppingCartService.addItem(this.product, this.quantity)
      .subscribe(
        () => this.addCartState = FeedbackButtonComponent.SUCCESS,
        () => this.addCartState = FeedbackButtonComponent.FAILED,
        () => {
          this.resetAddToCartState();
        }
      );
  }

  buyNow() {
    this.router.navigateByUrl(`/buy-now/${this.product.id}?quantity=${this.quantity}`);
  }

  addToFavorite() {
    if (this.addedFavorite) {
      return;
    }
    this.addingFavorite = true;
    this.favoriteService.add(this.product.id).subscribe(() => {
      this.cd.markForCheck();
      this.addingFavorite = false;
      this.addedFavorite = true;
      this.toastr.success('Product added to favorites');
    }, () => {

      this.toastr.success('Unknown error occurred while trying adding favorite');
    })
  }

  private resetAddToCartState() {
    this.cd.markForCheck();
    setTimeout(() => {
      this.addCartDisabled = false;
      this.addCartState = FeedbackButtonComponent.READY;
      this.cd.markForCheck()
    }, FeedbackButtonComponent.TIMEOUT)
  }


  private fetchSellerData() {
    this.accountService.fetchProfile(this.product.user_id)
      .subscribe(
        resp => {
          this.sellerInfo = resp;
          this.cd.markForCheck();
        }
      );
  }

  private resetProduct() {
    this.setPrimaryImage(0);
    this.quantity = 1;
    this.fetchSellerData();
  }
}
