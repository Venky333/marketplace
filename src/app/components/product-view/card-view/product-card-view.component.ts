import {Component, Input} from '@angular/core';

import {calcOffDiscount} from 'app/helpers/utils';
import {ProductService} from 'app/services/product';
import {Product} from 'app/models';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card-view.component.html',
  styleUrls: ['./product-card-view.component.scss']
})
export class ProductCardViewComponent {
  @Input() extraClass = '';
  @Input() product: Product;
  STATUS_PUBLISHED = ProductService.PRODUCT_STATUS_PUBLISHED;

  calcOffDiscount = calcOffDiscount;

}
