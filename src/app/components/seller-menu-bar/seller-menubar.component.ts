import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-seller-menubar',
  templateUrl: './seller-menubar.component.html'
})
export class SellerMenubarComponent {
  communityUrl: string;

  constructor() {
    this.communityUrl = environment.communityHomeUrl
  }
}
