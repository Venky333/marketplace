import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Category, CategoryService } from 'app/services/content';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-category-mobile-menu-bar',
  templateUrl: './category-mobile-menu-bar.component.html'
})
export class CategoryMobileMenuBarComponent {
  categories$: Observable<Array<Category>>;
  communityUrl: string;

  constructor(private categoryService: CategoryService) {
    this.categories$ = categoryService.getTop(true);
    this.communityUrl = environment.communityHomeUrl;
  }
}
