import {Component} from '@angular/core';
import {Product} from '../../models';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {calcOffDiscount} from '../../helpers/utils';
import {ShoppingCartService} from '../../services/shopping-cart';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-modal-view',
  templateUrl: './product-modal-view.component.html',
  styleUrls: ['./product-modal-view.component.scss']
})
export class ProductModalViewComponent {
  product: Product;
  quantity = 1;
  calcOffDiscount = calcOffDiscount;

  constructor(public activeModal: NgbActiveModal,
              private router: Router,
              private cartService: ShoppingCartService) {
  }

  handleQuantityUpdate(quantity: number) {
    this.quantity = quantity;
  }

  addToCart() {
    this.cartService.addItem(this.product, this.quantity).subscribe();
    this.activeModal.dismiss();
  }

  buyNow() {
    this.activeModal.dismiss();
    this.router.navigateByUrl(`/buy-now/${this.product.id}?quantity=${this.quantity}`);
  }

}
