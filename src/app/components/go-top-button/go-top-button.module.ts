import { NgModule } from '@angular/core';
import { GoTopButtonComponent } from './go-top-button.component';


@NgModule({
  declarations: [GoTopButtonComponent],
  exports: [GoTopButtonComponent]
})
export class GoTopButtonModule {

}
