import {Component, HostListener, Inject, Input} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {DOCUMENT} from '@angular/common';

import {WindowRefService} from 'app/services/window';

@Component({
  selector: 'app-go-top-button',
  template: `
    <button class="box-shadow-z3 dark go-top-button"
            [@appearInOut]="animationState"
            (click)="scrollTop($event)"></button>`,
  styles: [
      `
      .go-top-button {
        position: fixed;
        cursor: pointer;
        outline: none;
        right: 35px;
        bottom: 40px;
        background-color: white;
        background-image: url('../../../assets/images/up-arrow-bold-black.png');
        background-repeat: no-repeat;
        background-position: center;
        background-size: 14px;
        z-index: 20;
        width: 45px;
        height: 45px;
        border-radius: 50%;
        border: 1px solid #eee;

      }

      .go-top-button:hover, .go-top-button:focus {
        background-color: rgba(0, 0, 0, 0.3);
        text-decoration: none;
        color: white;
        background-image: url('../../../assets/images/up-arrow-bold-white.png');
      }`
  ],
  animations: [
    trigger('appearInOut', [
      state('in', style({
        'display': 'block',
        'opacity': '0.85'
      })),
      state('out', style({
        'display': 'none',
        'opacity': '0'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ],
})

/**
 * Component for adding a go-to-top button to scrollable browser content
 */
export class GoTopButtonComponent {
  animationState = 'out';

  private _window: Window;

  constructor(windowRef: WindowRefService, @Inject(DOCUMENT) private document: Document) {
    this._window = windowRef.nativeWindow;
  }

  /**
   * Go top button will appear when user scrolls Y to this position
   * @type {number}
   */
  @Input() scrollDistance = 200;


  /**
   * If true scrolling to top will be animated
   * @type {boolean}
   */
  @Input() animate = false;

  /**
   * Animated scrolling speed
   */
  @Input() speed = 80;

  /**
   * Acceleration coefficient, added to speed when using animated scroll
   * @type {number}
   */
  @Input() acceleration = 0;


  /**
   * Listens to window scroll and animates the button
   */
  @HostListener('window:scroll', [])
  onWindowScroll = () => {
    this.animationState = this.getCurrentScrollTop() > this.scrollDistance ? 'in' : 'out';
  }

  /**
   * Scrolls window to top
   * @param event
   */
  scrollTop = (event: any) => {


    event.preventDefault();
    if (this.animate) {
      this.animateScrollTop();
    } else {
      window.scrollTo(0, 0);
    }
  }

  /**
   * Performs the animated scroll to top
   */
  animateScrollTop = () => {
    let initialSpeed = this.speed;
    const timerID = setInterval(() => {
      window.scrollBy(0, -initialSpeed);
      initialSpeed = initialSpeed + this.acceleration;
      if (this.getCurrentScrollTop() === 0) {
        clearInterval(timerID);
      }
    }, 15);
  }

  /**
   * Get current Y scroll position
   * @returns {any|((event:any)=>undefined)}
   */
  getCurrentScrollTop = () => {
    if (typeof this._window.scrollY !== 'undefined') {
      return this._window.scrollY;
    }

    if (typeof this._window.pageYOffset !== 'undefined') {
      return this._window.pageYOffset;
    }

    if (typeof this.document.body.scrollTop !== 'undefined') {
      return this.document.body.scrollTop;
    }

    if (typeof this.document.documentElement.scrollTop !== 'undefined') {
      return this.document.documentElement.scrollTop;
    }

    return 0;
  }
}
