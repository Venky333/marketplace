import {Component, EventEmitter, Input, Output} from '@angular/core';

import {enforceNumber} from 'app/helpers/utils';

@Component({
  selector: 'app-quantity-input',
  templateUrl: './quantity-input.component.html',
  styleUrls: ['./quantity-input.component.scss']
})
export class QuantityInputComponent {
  @Output() update = new EventEmitter<number>();

  enforceNumber = enforceNumber;

  quantity: number;

  @Input() set value(value) {
    this.quantity = parseInt(value, 10);
  }

  updateValue(value) {
    if (!value || value === '0') {
      this.quantity = value;
      setTimeout(() => { // force update view
        this.update.emit(1);
        this.quantity = 1;
      }, 50);

    } else if ('decrease' === value) {
      if (this.quantity > 1) {
        this.update.emit(this.quantity - 1);
      }
    } else if ('increase' === value) {
      this.update.emit(this.quantity + 1);
    } else {
      this.update.emit(parseInt(value, 10));
    }
  }
}
