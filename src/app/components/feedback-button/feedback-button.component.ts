import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-feedback-button',
  templateUrl: './feedback-button.component.html',
  styleUrls: ['./feedback-button.component.scss'],
})
export class FeedbackButtonComponent implements OnChanges, OnInit {

  static TAG = 'FeedbackButtonComponent';

  static READY = 'ready';
  static WAITING = 'waiting';
  static SUCCESS = 'success';
  static FAILED = 'failed';
  static TIMEOUT = 3000;

  @Output() onClick = new EventEmitter<any>();

  /**
   * css classes to add to the feedback button. for example, "btn-sm btn-danger-invert"
   */
  @Input() btnClasses: string;

  /**
   * passed onto the [disabled] property of the button
   */
  @Input() disabled = false;

  /**
   * @type {string} text to shown when in failed state vs other states
   */
  @Input() defaultText = 'Save';
  @Input() failText: string;
  @Input() successText: string;
  @Input() waitingText: string;

  /**
   * @type {string} use the static constants
   */
  @Input() state: string = FeedbackButtonComponent.READY;


  buttonText: string;

  ngOnInit() {
    this.buttonText = this.defaultText;
    if (!this.failText) {
      this.failText = this.defaultText + ' failed';
    }
    if (!this.successText) {
      this.successText = this.defaultText;
    }
    if (!this.waitingText) {
      this.waitingText = this.defaultText;
    }
  }

  ngOnChanges() {
    if (this.state === FeedbackButtonComponent.SUCCESS) {
      this.buttonText = this.successText;
      setTimeout(() => {
        this.state = FeedbackButtonComponent.READY;
        this.buttonText = this.defaultText;
        this.disabled = false;
      }, FeedbackButtonComponent.TIMEOUT);

    } else if (this.state === FeedbackButtonComponent.FAILED) {
      this.buttonText = this.failText;
      this.disabled = true;
      setTimeout(() => {
        this.state = FeedbackButtonComponent.READY;
        this.disabled = false;
        this.buttonText = this.defaultText;
      }, FeedbackButtonComponent.TIMEOUT);
    } else if (this.state === FeedbackButtonComponent.READY) {
      this.buttonText = this.defaultText;
    } else if (this.state === FeedbackButtonComponent.WAITING) {
      this.buttonText = this.waitingText;
    }
  }

  handleClick() {
    if (!this.disabled) {
      this.onClick.emit();
    }
  }

}
