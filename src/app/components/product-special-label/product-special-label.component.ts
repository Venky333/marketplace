import {Component, Input} from '@angular/core';


@Component({
  selector: 'app-product-special-label',
  template: `
    <div *ngIf="product.is_special" class="product-special-label product-special-label--special">SPECIAL</div>
    <div *ngIf="product.is_clearance" class="product-special-label product-special-label--clearance">CLEARANCE</div>
  `,
  styleUrls: ['./product-special-label.component.scss']
})

export class ProductSpecialLabelComponent {

  @Input() product;


}
