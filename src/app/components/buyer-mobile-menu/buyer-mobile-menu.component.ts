import {Component, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../services/auth';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-buyer-mobile-menu',
  templateUrl: './buyer-mobile-menu.component.html'
})
export class BuyerMobileMenuComponent implements OnDestroy {
  communityUrl: string;
  loggedIn: boolean;
  private sub: Subscription;

  constructor(private authService: AuthService) {
    this.communityUrl = environment.communityHomeUrl;
    this.loggedIn = this.authService.isLoggedIn();
    this.sub = this.authService.tokenChangeBus.subscribe(() => {
      this.loggedIn = this.authService.isLoggedIn();
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
