import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-image-input',
  templateUrl: './image-input.component.html',
  styleUrls: ['./image-input.component.scss']
})
export class ImageInputComponent implements OnChanges {
  @Input() label: string;
  @Input() circle = false;
  @Input() image;
  @Output() imageSelected = new EventEmitter<File>();
  previewImage: string;

  handleSelectFile(input: HTMLInputElement) {
    const file: File = input.files[0];

    // Create an img element and add the image file data to it
    const img = document.createElement('img');
    img.src = window.URL.createObjectURL(file);
    let reader: any;
    reader = new FileReader();

    // Add an event listener to deal with the file when the reader is complete
    reader.addEventListener('load', (event) => {
      // Get the event.target.result from the reader (base64 of the image)
      img.src = event.target.result;

      const type = img.src.split(';')[0];
      input.value = null;
      if (type !== 'data:image/jpeg' && type !== 'data:image/png') {
        return;
      }
      this.previewImage = img.src;
      this.imageSelected.emit(file);

    }, false);

    reader.readAsDataURL(file);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('image') && this.image) {
      this.previewImage = this.image;
    }
  }


}
