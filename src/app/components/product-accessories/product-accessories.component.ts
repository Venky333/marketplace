import {Component, Input} from '@angular/core';

import {Observable} from 'rxjs';

import {Product} from 'app/models';
import {ProductService} from 'app/services/product';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-product-accessories',
  templateUrl: './product-accessories.component.html'
})
export class ProductAccessoriesComponent {
  productAccessories$: Observable<Array<Product>>;
  @Input() count = 2;

  /*TODO: change the service attributes/service itself to maintain productAcessories call */
  constructor(private productService: ProductService) {
    this.productAccessories$ = this.productService.listing(
      {
        sort: '-view_count',
        'per-page': this.count
      }
    ).pipe(map(resp => resp.products));
  }
}
