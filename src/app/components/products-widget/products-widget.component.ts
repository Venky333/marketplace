import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';

import {Product} from 'app/models';
import {ProductService} from 'app/services/product';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-products-widget',
  templateUrl: './products-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductsWidgetComponent implements OnChanges {
  products$: Observable<Array<Product>>;
  @Input() filters: any = {};
  @Input() title: string;
  @Input() moreLink: string;
  @Input() moreLinkParams = {};
  @Input() moreLabel = 'more >';
  @Input() extraClass = '';

  constructor(private productService: ProductService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('filters') && this.filters) {
      this.products$ = this.productService.listing(this.filters).pipe(map(resp => resp.products));
    }
  }
}
