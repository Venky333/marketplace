import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject, NgZone, OnDestroy, ViewChild} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {distinctUntilChanged, tap} from 'rxjs/operators';

import {environment} from '../../../environments/environment';
import {Category, CategoryService} from 'app/services/content';
import {autoClose} from 'app/helpers/autoclose';

@Component({
  selector: 'app-category-menubar',
  templateUrl: './category-menu-bar.component.html',
  styleUrls: ['./category-menu-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryMenuBarComponent implements OnDestroy {
  communityUrl: string;
  categories$: Observable<Array<Category>>;
  megaMenuItems: Category[] = [];
  megaMenuVisibility$ = new BehaviorSubject<boolean>(false);
  megaMenuOpen = false;
  @ViewChild('megaMenu') megaMenu: ElementRef<HTMLMainElement>;
  private activeElement: HTMLHtmlElement;
  private closed$ = new Subject<boolean>();

  constructor(private categoryService: CategoryService,
              private cd: ChangeDetectorRef,
              @Inject(DOCUMENT) private document: any,
              private ngZone: NgZone,
              private router: Router) {
    this.categories$ = this.categoryService.getTop(true);
    this.communityUrl = environment.communityHomeUrl;
    this.megaMenuVisibility$.pipe(
      distinctUntilChanged(),
      tap(value => {
        value ? this.open() : this.close();
      })
    ).subscribe()
  }

  handleItemClick(category, activeElement) {
    this.setMenuItems(category, activeElement);
    if (category.children.length) {
      this.megaMenuVisibility$.next(true);
    } else {
      this.megaMenuVisibility$.next(false);
      this.router.navigate(['/listing'], {queryParams: {category: category.id}});
    }
  }

  hideMenu() {
    this.megaMenuVisibility$.next(false);
  }

  showMenu(category: Category, activeElement) {
    this.setMenuItems(category, activeElement);
    if (!category || category.children.length) {
      this.megaMenuVisibility$.next(true);
    } else {
      this.megaMenuVisibility$.next(false);
    }
  }

  ngOnDestroy(): void {
    this.closed$.next();
  }

  private open() {
    if (!this.megaMenuOpen) {
      this.megaMenuOpen = true;
      this.setCloseHandlers();
      this.cd.markForCheck();
    }

  }

  private close() {
    if (this.megaMenuOpen) {
      this.megaMenuOpen = false;
      this.closed$.next(true);
      this.cd.markForCheck();
    }
  }

  private setCloseHandlers() {
    autoClose(this.ngZone,
      this.document, 'outside',
      () => this.hideMenu(),
      this.closed$,
      [this.megaMenu.nativeElement],
      [this.activeElement]
    );
  }

  private setMenuItems(category, activeElement) {
    if (category) {
      this.megaMenuItems = category.children;
      this.activeElement = activeElement;
      this.cd.markForCheck();
    }
  }
}
