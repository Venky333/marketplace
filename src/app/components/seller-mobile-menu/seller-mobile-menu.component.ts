import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-seller-mobile-menu',
  templateUrl: './seller-mobile-menu.component.html'
})
export class SellerMobileMenuComponent {
  communityUrl: string;

  constructor() {
    this.communityUrl = environment.communityHomeUrl
  }

}
