import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs';

import {ProductService} from 'app/services/product';
import {Product} from 'app/models';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-related-products-widget',
  templateUrl: './related-products.component.html'
})

export class RelatedProductsWidgetComponent {
  relatedProducts$: Observable<Array<Product>>;
  @Input() count = 3;

  constructor(private productService: ProductService) {
    this.relatedProducts$ = this.productService.listing({
      sort: '-view_count',
      'per-page': this.count,
    }).pipe(
      map(resp => resp.products)
    );
  }
}
