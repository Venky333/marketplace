import { NgModule } from '@angular/core';
import { DriftZoomDirective } from './drift-zoom.directive';

@NgModule({
  declarations: [
    DriftZoomDirective
  ],
  exports: [
    DriftZoomDirective
  ]
})
export class DriftZoomModule {

}