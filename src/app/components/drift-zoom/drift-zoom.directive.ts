import { AfterViewInit, Directive, ElementRef, OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import * as Drift from 'drift-zoom';

@Directive({
  selector: '[driftZoom]'
})
export class DriftZoomDirective implements AfterViewInit, OnDestroy {

  @Input() paneRef: any;

  private drift;

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    this.drift = new Drift(this.el.nativeElement, {
      paneContainer: this.paneRef,
      zoomFactor: 2
    });
  }

  ngOnDestroy() {
    if (this.drift) {
      this.drift.destroy();
    }
  }
}
