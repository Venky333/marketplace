import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-order-status-icon',
  template: `
    <div class="order-status-icon">
      <img src="assets/images/shipped-colored.svg" ngbTooltip="Order fully shipped"
           *ngIf="order.is_completed; else processingIcon"
           tooltipClass="shipped order-state" container="body" placement="top-left">
    </div>
    <ng-template #processingIcon>
      <img src="assets/images/overdue-colored.svg" ngbTooltip="Order being processed"
           tooltipClass="overdue order-state" container="body" placement="top-left">
    </ng-template>

  `,
  styleUrls: ['./order-status-icon.component.scss']
})
export class OrderStatusIconComponent {
  @Input() order;

}
