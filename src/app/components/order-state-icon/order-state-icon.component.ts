import {Component, Input} from '@angular/core';
import {SellerOrderService} from '../../pages/seller/order/seller-order.service';

@Component({
  selector: 'app-order-state-icon',
  template: `
    <ng-container [ngSwitch]="state">
      <div *ngSwitchCase="STATE_NEW"
           ngbTooltip="NEW ORDER" tooltipClass="new order-state" container="body" placement="top"
           class="order-state-icon order-state-icon--new">
        <img src="assets/images/star-colored.svg" alt="New Order">
      </div>
      <div *ngSwitchCase="STATE_OVERDUE"
           ngbTooltip="OVERDUE" tooltipClass="overdue order-state" container="body" placement="top"
           class="order-state-icon order-state-icon--overdue">
        <img src="assets/images/overdue-colored.svg" alt="Overdue">
      </div>
      <div *ngSwitchCase="STATE_SHIPPED"
           ngbTooltip="SHIPPED" tooltipClass="shipped order-state" container="body" placement="top"
           class="order-state-icon order-state-icon--shipped">
        <img src="assets/images/shipped-colored.svg" alt="Shipped">
      </div>
      <div *ngSwitchCase="STATE_RETURN"
           ngbTooltip="RETURN" tooltipClass="return order-state" container="body" placement="top"
           class="order-state-icon order-state-icon--return">
        <img src="assets/images/return-colored.svg" alt="Return">
      </div>
      <div *ngSwitchCase="STATE_SOLD_OUT"
           ngbTooltip="SOLD OUT" tooltipClass="sold-out order-state" container="body" placement="top"
           class="order-state-icon order-state-icon--sold-out">
        <img src="assets/images/sold-out-colored.svg" alt="Sold out">
      </div>
    </ng-container>
  `,
  styleUrls: ['./order-state-icon.component.scss']
})
export class OrderStateIconComponent {
  @Input() state;

  STATE_NEW = SellerOrderService.STATE_NEW;
  STATE_OVERDUE = SellerOrderService.STATE_OVERDUE;
  STATE_SHIPPED = SellerOrderService.STATE_SHIPPED;
  STATE_RETURN = SellerOrderService.STATE_RETURN;
  STATE_SOLD_OUT = SellerOrderService.STATE_SOLD_OUT;

}
