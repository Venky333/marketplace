import {Component, EventEmitter, Inject, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {AUTH_SERVICE_CONFIG, AuthService} from 'app/services/auth';
import {ShoppingCartService} from 'app/services/shopping-cart'
import {OauthUrls} from 'app/models/oauth-urls';
import {AccountService} from 'app/services/account/account.service';
import {User} from 'app/models';
import {FavoritesService} from 'app/services/favorites.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnDestroy, OnChanges {

  static TAG = 'AppHeaderComponent';

  registerUrl: string;
  logoutUrl: string;
  loginUrl: string;
  cartItemCount: number;
  favoritesCount$: Observable<number>;
  @Input() logoUrl: string;
  @Input() isSeller: boolean;
  @Input() user: User;
  @Output() searchClicked = new EventEmitter<string>();
  @Output() logout = new EventEmitter<string>();

  private subs: Array<Subscription>;
  private profiles$: Observable<any[]>;

  constructor(@Inject(AUTH_SERVICE_CONFIG) public authConfig,
              private authService: AuthService,
              private accountService: AccountService,
              private modalService: NgbModal,
              private favoriteService: FavoritesService,
              private shoppingCartService: ShoppingCartService) {

    this.cartItemCount = this.shoppingCartService.getItemsCount();
    this.favoritesCount$ = this.favoriteService.count();
    this.authService.getOauthUrl().subscribe((urls: OauthUrls) => {
      this.registerUrl = urls.registerUrl;
      this.logoutUrl = urls.logoutUrl;
      this.loginUrl = urls.loginUrl;
    });
    this.subs = [
      this.shoppingCartService.shoppingCartChangeBus.subscribe(() => this.cartItemCount = this.shoppingCartService.getItemsCount()),
      this.favoriteService.favoritesUpdated.subscribe(() => this.favoritesCount$ = this.favoriteService.count())
    ];
  }

  openSearch() {
    this.searchClicked.emit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('user') && this.user && !this.profiles$) {
      this.profiles$ = this.accountService.getAllProfiles().pipe(
        map(profiles => {
          return profiles.filter(p => this.user.profile_id !== p.profile_id);
        })
      )

    }
  }

  logoutUser() {
    this.logout.emit(this.logoutUrl);
  }

  ngOnDestroy(): void {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
