import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthService} from 'app/services/auth';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-buyer-menubar',
  templateUrl: './buyer-menubar.component.html'
})
export class BuyerMenubarComponent implements OnDestroy {
  communityUrl: string;
  loggedIn: boolean;
  private sub: Subscription;

  constructor(private authService: AuthService) {
    this.communityUrl = environment.communityHomeUrl;
    this.loggedIn = this.authService.isLoggedIn();
    this.sub = this.authService.tokenChangeBus.subscribe(() => {
      this.loggedIn = this.authService.isLoggedIn();
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
