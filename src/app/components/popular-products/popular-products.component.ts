import {Component} from '@angular/core';

@Component({
  selector: 'app-popular-products',
  template: `
    <app-products-widget [filters]="filters" title="Popular right now" moreLink="/listing"
                         extraClass="popular-products-widget"></app-products-widget>
  `,
})
export class PopularProductsComponent {
  filters = {
    sort: '-view_count',
    'per-page': 4,
  };
}
