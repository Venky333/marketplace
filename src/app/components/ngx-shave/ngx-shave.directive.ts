import {AfterViewInit, Directive, ElementRef, Input, OnDestroy, OnInit} from '@angular/core';
import shave from 'shave';
import {WindowRefService} from '../../services/window';
import {fromEvent, Subject} from 'rxjs';
import {debounceTime, takeUntil} from 'rxjs/operators';

@Directive({
  selector: '[ngxShave]'
})
export class NgxShaveDirective implements AfterViewInit, OnInit, OnDestroy {
  @Input() maxHeight;
  @Input() options = {};
  @Input() listenResize = false;
  private destroyed$ = new Subject();

  constructor(private el: ElementRef, private windowService: WindowRefService) {
  }

  ngOnInit() {
    if (this.listenResize) {
      this.installResizeListener();
    }
  }

  ngAfterViewInit(): void {
    this.renderShave();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private installResizeListener() {
    fromEvent(this.windowService.nativeWindow, 'resize')
      .pipe(
        takeUntil(this.destroyed$),
        debounceTime(50)
      )
      .subscribe(() => this.renderShave())
  }

  private renderShave() {
    if (this.maxHeight) {
      shave(this.el.nativeElement, this.maxHeight, this.options)
    }
  }

}
