import {Component, Input} from '@angular/core';
import {SellerOrderService} from '../../pages/seller/order/seller-order.service';
import {SubOrder} from 'app/models/sub-order';

@Component({
  selector: 'app-order-state-label',
  template: `
    <ng-container [ngSwitch]="order.state">
      <div *ngSwitchCase="STATE_NEW" class="order-state order-state--unprocessed">UNPROCESSED</div>
      <div *ngSwitchCase="STATE_OVERDUE" class="order-state order-state--overdue">OVERDUE</div>
      <div *ngSwitchCase="STATE_SOLD_OUT" class="order-state order-state--sold-out">SOLD OUT</div>
      <div *ngSwitchDefault class="order-state">
        <div class="text-lowercase">{{order.created_at | date:'shortTime'}}</div>
        {{order.updated_at | date:'d MMM yyyy'}}
      </div>
    </ng-container>
  `,
  styleUrls: ['./order-state-label.component.scss']
})
export class OrderStateLabelComponent {
  @Input() order: SubOrder;

  STATE_NEW = SellerOrderService.STATE_NEW;
  STATE_OVERDUE = SellerOrderService.STATE_OVERDUE;
  STATE_SOLD_OUT = SellerOrderService.STATE_SOLD_OUT;

}
