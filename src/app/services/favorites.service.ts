import {Injectable} from '@angular/core';
import {map, shareReplay, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {merge, Observable, Subject} from 'rxjs';
import {ParentService} from './parent';
import {environment} from '../../environments/environment';
import {ProductService} from './product';
import {AccountService} from './account/account.service';
import {AuthService} from './auth';

@Injectable({
  'providedIn': 'root'
})
export class FavoritesService extends ParentService {
  static TAG = 'FavoritesService';
  favoritesUpdated = new Subject();
  private countCache$: Observable<any>;

  constructor(private http: HttpClient, private authService: AuthService) {
    super(FavoritesService.TAG);
    this.api = environment.apiBaseUrl + '/api/v1/favorites';
    merge(this.authService.switchAccountStarted, this.favoritesUpdated).subscribe(() => {
      this.countCache$ = null;
    })
  }

  public count() {
    if (!this.countCache$) {
      this.countCache$ = this.requestCount().pipe(
        shareReplay(1)
      );
    }

    return this.countCache$;
  }

  public query(params) {
    return this.http.get(`${this.api}?${this.queryString(params)}`)
      .pipe(
        map((resp: any) => {
          resp.favorites = resp.favorites.map(f => {
            f.product = ProductService.transformProduct(f.product);
            return f;
          });
          return resp;
        })
      );
  }

  public add(product_id) {
    return this.http.post(`${this.api}`, {product_id})
      .pipe(
        tap(() => this.favoritesUpdated.next())
      )
  }

  public remove(id) {
    return this.http.delete(`${this.api}/${id}`)
      .pipe(
        tap(() => this.favoritesUpdated.next())
      );
  }

  public find(id) {
    return this.http.get(`${this.api}/${id}`);
  }

  private requestCount() {
    return this.http.get<any>(this.api)
      .pipe(
        map(resp => resp.meta.total)
      )

  }
}
