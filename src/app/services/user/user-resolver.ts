import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthService} from 'app/services/auth/service';
import {AccountService} from 'app/services/account/account.service';

@Injectable({'providedIn': 'root'})
export class UserResolver implements Resolve<any> {

  static TAG = 'UserResolver';

  constructor(private authService: AuthService,
              private accountService: AccountService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    const user = this.accountService.getProfile();
    if (user) {
      return user;
    }
    const token = this.authService.getToken();
    if (token) {
      console.debug(UserResolver.TAG, 'token found, retrieving user');
      this.authService.startSession(token);
      return this.accountService.fetch(false).catch((fail: Response) => {
        // clears cache;
        console.error(UserResolver.TAG, 'user retrieval failed', fail);
        this.authService.logout();
        if (fail.status === 500) {
          this.router.navigateByUrl('/app-error');
        }
      });
    }
  }

}
