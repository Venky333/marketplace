import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {catchError, map, shareReplay, tap} from 'rxjs/operators';

import {environment} from '../../../environments/environment';
import {AuthToken, User} from 'app/models';
import {AuthService} from '../auth';
import {ParentService} from '../parent';

@Injectable({providedIn: 'root'})
export class AccountService extends ParentService {
  static TAG = 'AccountService';
  static PUBLISHED_PRODUCT_QUOTA = 'published_products';

  /**
   * emits even when edits to ... are made
   */
  personalizeChangeBus: Subject<any> = new Subject<any>();

  private profilePromise: Promise<any>;
  private profilesCache = new Map<number, any>();
  private profile: any;
  private token: string;
  private accountApiBaseUrl: string;
  private apiBaseUrl: string;
  private corporateTypesCache: any;
  private industriesCache: any;

  constructor(private authService: AuthService,
              private http: HttpClient) {
    super(AccountService.TAG);
    this.accountApiBaseUrl = environment.accountApiBaseUrl;
    this.apiBaseUrl = environment.apiBaseUrl;
    if (authService.isLoggedIn()) {
      this.initToken();
    }

    this.authService.switchAccountStarted.subscribe(() => this.clearCache());
    this.authService.tokenChangeBus.subscribe((token: AuthToken) => {
      console.debug(AccountService.TAG, 'user change detected:', token ? 'init user' : 'clearing cache');
      this.initToken();
      token ? this.initUser(token) : this.clearCache();
    });

  }

  /**
   * @returns {User} shallow copy of user
   */
  getProfile() {
    return this.profile ? JSON.parse(JSON.stringify(this.profile)) : null;
  }

  fetch(forceRefresh: boolean = false) {

    if (!this.profilePromise || forceRefresh) {
      const uri = `${this.accountApiBaseUrl}/profiles/${this.authService.getToken().userId}`;
      this.profilePromise = this.http.get<any>(uri, this.getAccountAuthHeaders()).pipe(
        map(resp => this.setProfile(resp)))
        .toPromise()
        .catch((fail) => {
          this.profilePromise = null;
          return this.handlePromiseError(fail);
        });
    }
    return this.profile ? Promise.resolve(this.getProfile()) : this.profilePromise;
  }

  getAllProfiles() {
    return this.http.get(`${this.accountApiBaseUrl}/users/me/profiles`, this.getAccountAuthHeaders())
      .pipe(map((resp: any) => resp.data.sort((a, b) =>
        (new Date(a.updated_at) < new Date(b.updated_at))
          ? 1
          : ((new Date(b.updated_at) < new Date(a.updated_at)) ? -1 : 0))
      ));
  }

  update(data) {
    return this.http.patch(`${this.accountApiBaseUrl}/profiles/${this.authService.getToken().userId}`, data, this.getAccountAuthHeaders())
      .pipe(tap(resp => this.setProfile(resp)));
  }

  changePassword(payload) {
    return this.http.post(`${this.accountApiBaseUrl}/users/me/change-password`, payload, this.getAccountAuthHeaders())
  }

  getAuthorizedApps() {
    return this.http.get(`${this.accountApiBaseUrl}/users/me/apps`, this.getAccountAuthHeaders())
      .pipe(map((resp: any) => resp.data));
  }

  private setProfile(resp) {
    this.profile = resp.data;
    this.personalizeChangeBus.next();
    return this.getProfile();
  }

  private getAccountAuthHeaders() {
    return {headers: {'Authorization': this.token}};
  }


  uploadImage(image, name) {
    const formData = new FormData();
    formData.append(name, image);

    return this.http.post(`${this.accountApiBaseUrl}/profiles/${this.authService.getToken().userId}/image`,
      formData,
      this.getAccountAuthHeaders()
    );
  }

  fetchIndustries(): Observable<any> {
    if (this.industriesCache) {
      return of(this.industriesCache);
    }
    return this.http.get<any>(`${this.accountApiBaseUrl}/industries`, this.getAccountAuthHeaders())
      .pipe(
        map(resp => {
          this.industriesCache = resp.data;
          return this.industriesCache;
        })
      )
  }

  fetchCorporateType(): Observable<any> {
    if (this.corporateTypesCache) {
      return of(this.corporateTypesCache);
    }
    return this.http.get<any>(`${this.accountApiBaseUrl}/corporate-types`, this.getAccountAuthHeaders())
      .pipe(
        map(resp => {
          this.corporateTypesCache = resp.data;
          return this.corporateTypesCache;
        })
      )
  }

  fetchQuota() {
    return this.http.get(`${this.apiBaseUrl}/api/v1/account/quota`).pipe(
      map((resp: any) => resp.quota)
    );
  }

  fetchAddress() {
    return this.http.get<any>(`${this.apiBaseUrl}/api/v1/account/address`);
  }

  updateAddress(data) {
    return this.http.post(`${this.apiBaseUrl}/api/v1/account/address`, data);
  }

  fetchProfile(user_id: number): Observable<User> {
    if (!this.profilesCache.has(user_id)) {

      if (this.profilesCache.size > 64) {
        this.profilesCache.clear();
      }

      this.profilesCache.set(user_id,
        this.http.get(`${this.accountApiBaseUrl}/profiles/${user_id}`)
          .pipe(
            map((resp: any) => resp.data),
            catchError(this.handleObsError.bind(this)),
            shareReplay(1)
          )
      );
    }

    return this.profilesCache.get(user_id);
  }

  private initUser(token: AuthToken) {
    this.fetch(true).catch(() => {
    });
  }

  private clearCache() {
    this.profilePromise = null;
    this.profile = null;
  }

  private initToken() {
    this.token = `Bearer ${this.authService.getToken().accountsApiToken}`;
  }
}
