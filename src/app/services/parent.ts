import { throwError as observableThrowError, Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

export class ParentService {

  protected api: string;

  static addMeta(data: any, resp: HttpResponse<any>): any {
    return {
      items: data,
      meta: {
        page: resp.headers.get('X-Pagination-Current-Page'),
        pageSize: resp.headers.get('X-Pagination-Per-Page'),
        totalCount: resp.headers.get('X-Pagination-Total-Count'),
        pageCount: resp.headers.get('X-Pagination-Page-Count'),
      }
    };
  }

  constructor(private tag: string) {
  }


  protected handleObsError(error: HttpResponse<any>): Observable<any> {
    console.error(this.tag, error);
    return observableThrowError(error);
  }

  protected handlePromiseError(error: HttpResponse<any>): Promise<any> {
    console.error(this.tag, error);
    return Promise.reject(error);
  }

  protected queryString(queryParams: any) {
    return (queryParams) ? Object.keys(queryParams).map(query => {
      return (Array.isArray(queryParams[query]) && queryParams[query].length > 0) ?
        queryParams[query].map(value => `${query}[]=${value}`).join('&')
        : `${query}=${queryParams[query]}`;
    }).join('&') : '';
  }

}
