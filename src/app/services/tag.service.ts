import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of as observableOf} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {ParentService} from './parent';

@Injectable({'providedIn': 'root'})
export class TagService extends ParentService {
  static TAG = 'TagService';
  private cache: Map<string, any> = new Map<string, any>();

  constructor(private http: HttpClient) {
    super(TagService.TAG);
    this.api = '/api/v1/tags';
  }

  fetch(id: string): Observable<any> {
    return this.cacheData(`${this.api}/${id}`);
  }

  private cacheData(url): Observable<any> {
    if (this.cache.has(url)) {
      return observableOf(this.cache.get(url));
    }
    return this.http.get(url).pipe(
      map((resp) => {
        this.cache.set(url, <any>resp);
        return resp;
      }),
      catchError(this.handleObsError.bind(this))
    );
  }
}
