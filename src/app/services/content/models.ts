export interface Category {
  id: number,
  name: string,
  parent_id: number,
  product_count: number,
  slug: string,
  children?: Category[]
}
