import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of as observableOf} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {environment} from '../../../environments/environment';
import {Category} from './models';
import {ParentService} from '../parent';

@Injectable({'providedIn': 'root'})
export class CategoryService extends ParentService {
  static TAG = 'CategoryService';

  private categoryCache: Map<string, Array<Category>> = new Map<string, Array<Category>>();

  constructor(
    private http: HttpClient) {
    super(CategoryService.TAG);
    this.api = environment.accountApiBaseUrl;
  }

  query(): Observable<Array<Category>> {
    return this.cacheCategory('/api/v1/categories').pipe(
      map((resp: any) => resp.categories)
    );
  }

  suggest(queryParams): Observable<Array<Category>> {
    console.debug(CategoryService.TAG + ' search', queryParams);
    return this.cacheCategory('/api/v1/suggest/category?' + this.queryString(queryParams));
  }

  fetch(id): Observable<Category> {
    return this.cacheCategory('/api/v1/suggest/category?ids=' + id).pipe(
      map(data => data.length ? data[0] : data)
    )
  }

  getTop(includeChildren = false): Observable<Array<Category>> {

    return this.cacheCategory(`/api/v1/categories/top` + (includeChildren ? '?expand=children' : ''))
      .pipe(
        map((resp: any) => resp.categories)
      );
  }

  private cacheCategory(url): Observable<any> {
    if (this.categoryCache.get(url)) {
      return observableOf(this.categoryCache.get(url));
    } else {
      return this.http.get(url)
        .pipe(
          map((resp) => {
            this.categoryCache.set(url, <any>resp);
            return resp;
          }),
          catchError(this.handleObsError.bind(this))
        );
    }
  }

}
