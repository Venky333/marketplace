import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {createSlug} from 'app/helpers/utils';
import {Product} from 'app/models';
import {ProductLabel} from 'app/models/product-label';
import {ParentService} from '../parent';

@Injectable({'providedIn': 'root'})
export class ProductService extends ParentService {

  static TAG = 'SellerProductService';

  static PRODUCT_STATUS_PUBLISHED = 10;

  static PRODUCT_STATUS_PENDING = 20;

  static PRODUCT_STATUS_DRAFT = 30;

  static PRODUCT_STATUS_WITHDRAWN = 40;

  static PRODUCT_STATUS_REJECTED = 50;

  static PRODUCT_STATUS_ARCHIVED = 60;

  static PRODUCT_STATUS_PUBLISHED_COMMUNITY = 70;

  static PRODUCT_STATUS_REMOVED = 90;

  private labels: any;

  static transformProduct(product: Product) {
    if (!product) {
      return null;
    }
    product.link = createSlug(product.name);
    product.is_special = product.labels.some(l => l.type === ProductLabel.SPECIAL);
    product.is_clearance = product.labels.some(l => l.type === ProductLabel.CLEARANCE);
    return product;
  }

  constructor(private http: HttpClient) {
    super(ProductService.TAG);
    this.api = '/api/v1/products';
  }

  get statusLabels() {
    if (!this.labels) {
      this.labels = {};
      this.labels[ProductService.PRODUCT_STATUS_PUBLISHED] = 'POSTED for SALE';
      this.labels[ProductService.PRODUCT_STATUS_WITHDRAWN] = 'WITHDRAWN';
      this.labels[ProductService.PRODUCT_STATUS_PENDING] = 'PENDING';
      this.labels[ProductService.PRODUCT_STATUS_DRAFT] = 'DRAFT';
      this.labels[ProductService.PRODUCT_STATUS_PUBLISHED_COMMUNITY] = 'DRAFT';
      this.labels[ProductService.PRODUCT_STATUS_REJECTED] = 'REJECTED';
      this.labels[ProductService.PRODUCT_STATUS_REMOVED] = 'DELETED';
    }
    return this.labels;
  }

  fetch(id: string, expand: Array<String> = [], queryParams: any = null): Promise<any | Product> {
    let expandString = expand ? '?expand=' + expand.join(',') : '';
    expandString = expandString + (expandString ? '&' : '?') + this.queryString(queryParams);
    return this.http.get(`${this.api}/${id}${expandString}`)
      .pipe(map((resp: any) => ProductService.transformProduct(resp.product)))
      .toPromise()
      .catch(this.handlePromiseError.bind(this));
  }

  latest(count: number): Observable<any> {
    return this.query(`${this.api}`, {'per-page': count, sort: '-published_at'}).pipe(
      map(resp => resp.products)
    );
  }

  related(count: number, product_id: number) {
    return this.query(this.api, {'per-page': count, related: product_id}).pipe(map(resp => resp.products));
  }

  suggest(count: number, product_id: number = null): Observable<any> {
    const queryParams = {'per-page': count};
    if (null != product_id) {
      queryParams['product_id'] = product_id;
    }
    return this.query(`${this.api}/${product_id}/suggest`, queryParams).pipe(map(resp => resp.products));
  }

  listing(queryParams: any = null) {
    return this.query(`${this.api}`, queryParams);
  }

  query(basePath: string, queryParams: any = {}): Observable<any> {
    queryParams.status = [ProductService.PRODUCT_STATUS_PUBLISHED];
    const qpString = this.queryString(queryParams);
    const url = `${basePath}?${qpString}`;
    return this.http.get<any>(url).pipe(
      map(resp => {
        resp.products = resp.products.map(product => ProductService.transformProduct(product));
        return resp;
      }),
      catchError(this.handleObsError.bind(this))
    );
  }

}
