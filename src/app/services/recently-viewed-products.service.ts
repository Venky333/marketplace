import {Injectable} from '@angular/core';
import {WindowRefService} from './window';
import {ProductService} from './product';
import {Product} from '../models';
import {Observable, of as observableOf} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({'providedIn': 'root'})
export class RecentlyViewedProductsService {
  static TOKEN_KEY = 'recentlyViewed';
  static LIMIT = 32;
  static TAG = 'RecentlyViewedProductsService';
  window: Window;

  private product_ids = [];

  constructor(windowRef: WindowRefService, private productService: ProductService) {
    this.window = windowRef.nativeWindow;
    this.retrieveProductIds();
  }

  addProduct(product: Product) {
    this.product_ids.unshift(product.id);
    if (this.product_ids.length > RecentlyViewedProductsService.LIMIT) {
      this.product_ids = this.product_ids.slice(0, RecentlyViewedProductsService.LIMIT);
    }
    // remove duplicate
    const index = this.product_ids.indexOf(product.id, 1);
    if (index !== -1) {
      this.product_ids.splice(index, 1);
    }
    this.saveProductIds();
  }

  getProducts(limit = 6): Observable<Array<Product | any>> {
    if (this.product_ids.length) {
      const product_ids = this.product_ids.slice(0, limit);
      return this.productService.listing({id__in: product_ids.join(',')})
        .pipe(
          map(// show latest viewed product first
            resp => resp.products
              .map(item => [product_ids.indexOf(item.id), item])
              .sort()
              .map(j => j[1])
          )
        );
    }
    return observableOf([]);
  }

  recentlyViewedCount() {
    return this.product_ids.length;
  }

  private retrieveProductIds() {
    try {
      const product_ids = this.window.localStorage.getItem(RecentlyViewedProductsService.TOKEN_KEY);
      this.product_ids = product_ids ? JSON.parse(product_ids) : [];

    } catch (e) {
      console.error(RecentlyViewedProductsService.TAG, 'retrieveProductIds()', e);
      this.product_ids = [];
    }
    return this.product_ids;
  }


  private saveProductIds() {
    this.window.localStorage.setItem(RecentlyViewedProductsService.TOKEN_KEY, JSON.stringify(this.product_ids));
  }
}
