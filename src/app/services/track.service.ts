import {ParentService} from './parent';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class TrackService extends ParentService {
  static TAG = 'TrackService';

  constructor(private http: HttpClient) {
    super(TrackService.TAG);
    this.api = `${environment.apiBaseUrl}/api/v1/track`;
  }

  productView(product_id: number) {
    return this.http.post(this.api, {event: 'product_view', data: {product_id}});
  }
}
