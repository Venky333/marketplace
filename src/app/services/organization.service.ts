import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(private http: HttpClient) {
  }

  fetchOrganization(product_id: any) {
    return this.http.get(`${environment.communityApiBaseUrl}/v2/organizations/find-by-profile?expand=country&profile_id=` + product_id);
  }

  fetchProfile(profile_id: number) {
    return this.http.get(`${environment.communityApiBaseUrl}/v2/profiles/${profile_id}`);
  }

}
