import {Injectable} from '@angular/core';
import {Observable, of as observableOf} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {ParentService} from './parent';
import {ShippingServiceModel} from '../models/shipping-service-model';

@Injectable({providedIn: 'root'})
export class ShippingService extends ParentService {

  public static TAG = 'ShippingServiceModel';
  private shippingServicesCache: ShippingServiceModel[];

  constructor(private http: HttpClient) {
    super(ShippingService.TAG);
    this.api = `${environment.apiBaseUrl}/api/v1/shipping-services`;
  }

  fetch(): Observable<ShippingServiceModel[]> {
    if (this.shippingServicesCache) {
      return observableOf(this.shippingServicesCache);
    }
    return this.http.get<any>(this.api)
      .pipe(map(result => {
        this.shippingServicesCache = result;
        return this.shippingServicesCache;
      }));
  }
}

