import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject, timer, of as observableOf} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {zip} from 'rxjs/internal/observable/zip';

import {ParentService} from '../parent';
import {AuthService} from '../auth';
import {WindowRefService} from '../window';
import {Product} from 'app/models/product';
import {CartItem, ShoppingCartChangeEvent} from './models';
import {ProductService} from '../product';

@Injectable({'providedIn': 'root'})
export class ShoppingCartService extends ParentService {

  static TAG = 'ShoppingCartService';
  static ITEMS_KEY = 'cartItems';

  shoppingCartChangeBus: Subject<ShoppingCartChangeEvent> = new Subject<ShoppingCartChangeEvent>();

  private items: Array<CartItem> = null;
  private window: Window;
  private promise: Promise<Array<CartItem>>;
  private total: number;
  private itemsCount: number;

  static calculateTotals(items: Array<CartItem>): { total: number, itemsCount: number } {
    let total = 0, itemsCount = 0;
    if (items) {
      items.map(item => {
        total += (item.quantity * (item.product.discount_price ? item.product.discount_price : item.product.price));
        itemsCount += item.quantity;
      });
    }
    return {total: total, itemsCount: itemsCount};
  }

  constructor(private http: HttpClient,
              windowRef: WindowRefService,
              private authService: AuthService) {
    super(ShoppingCartService.TAG);
    this.api = '/api/v1/cart';
    this.window = windowRef.nativeWindow;
    this.shoppingCartChangeBus.subscribe(() => {
      this.calcTotals();
    });
    this.authService.switchAccountStarted.subscribe(() => {
      this.emptyCart();
    });
    this.authService.tokenChangeBus.subscribe(() => {
      this.promise = null;
      if (this.authService.isLoggedIn()) {
        this.syncItems();
      } else {
        this.emptyCart();
      }
    });

    this.retrieveItems();

  }

  /**
   *
   * @returns resolves to a shallow copy of the internal list. client may manipulate that array without changing internals.
   */
  getItems() {
    if (this.items !== null) {
      return this.items.slice();
    }
    return [];
  }

  setItems(items) {
    this.items = items.slice();
    this.storeItems(this.items);
    this.shoppingCartChangeBus.next({items: this.items.slice()});
    return this.storeRemote();
  }

  getTotal() {
    if (!this.total) {
      this.calcTotals();
    }
    return this.total;
  }

  getItemsCount() {
    return this.itemsCount;
  }

  /**
   *  Adds or updates if exist product to cart
   * @param product
   * @param quantity
   */
  addItem(product: Product, quantity: number = 1): Observable<any> {
    const index = this.items.findIndex(item => {
      return item.product_id === product.id;
    });
    if (index !== -1) {
      quantity += this.items[index].quantity;
    }
    let obs: Observable<any> = timer(2000); // to emulate adding time
    if (this.authService.isLoggedIn()) {
      obs = zip(this.addItemToRemote(product.id, quantity), obs);
    }

    return obs.pipe(
      tap(() => {
        if (index !== -1) {
          this.items[index].quantity = quantity;
        } else {
          this.items.push({product, quantity, product_id: product.id});
        }
        this.storeItems(this.items);
        this.shoppingCartChangeBus.next({items: this.items.slice()});
      })
    );

  }


  private calcTotals() {
    const {total, itemsCount} = ShoppingCartService.calculateTotals(this.items);
    this.total = total;
    this.itemsCount = itemsCount;
  }

  private addItemToRemote(product_id: number, quantity: number) {
    return this.bulkAddItemsToRemote([{product_id, quantity}]);
  }

  private bulkAddItemsToRemote(items: Array<{ product_id: number, quantity: number }>) {
    return this.http.post(`${this.api}/add`, {items}).pipe(catchError(this.handleObsError.bind(this)));
  }

  private emptyCart() {
    this.items = [];
    this.storeItems([]);
    this.shoppingCartChangeBus.next({items: []});
  }

  private retrieveItems() {
    if (this.authService.isLoggedIn()) {
      this.syncItems();
    } else {
      this.items = this.retrieveItemsFromStorage();
      this.calcTotals();
    }
  }

  private syncItems() {
    this.retrieveItemsFromServer().then((items: any) => {
      if (items.length) {
        this.storeItems(items);
      } else {
        items = this.retrieveItemsFromStorage();
        if (items.length) {
          this.bulkAddItemsToRemote(items.map(item => {
            return {product_id: item.product_id, quantity: item.quantity};
          })).subscribe(() => {
          });
        }
      }
      this.items = items;
      this.shoppingCartChangeBus.next({items: this.items.slice()});
    }).catch(() => {
      this.items = [];
      this.storeItems([]);
      this.shoppingCartChangeBus.next({items: []})
    });

  }

  /**
   *  Retrieves from browser's local storage
   */
  private retrieveItemsFromStorage() {
    let items;
    try {
      const token = this.window.localStorage.getItem(ShoppingCartService.ITEMS_KEY);
      items = token ? JSON.parse(token) : [];
    } catch (e) {
      console.error(ShoppingCartService.TAG, 'getItems()', e);
      items = [];
      this.storeItems(items);
    }
    return items;
  }

  private retrieveItemsFromServer(): Promise<Array<CartItem>> {
    if (!this.promise) {
      this.promise = this.http.get(`${this.api}`).pipe(
        map((resp: any) => {
          const items: Array<CartItem> = resp.data;
          items.forEach((item: CartItem) => {
            item.product = ProductService.transformProduct(item.product);
            return item;
          });
          return items;
        }))
        .toPromise().catch(this.handlePromiseError.bind(this));
    }
    return this.promise;
  }

  private storeRemote() {
    return this.authService.isLoggedIn() ? this.http.put(`${this.api}`, {items: this.items}) : observableOf(null);
  }

  private storeItems(items) {
    this.window.localStorage.setItem(ShoppingCartService.ITEMS_KEY, JSON.stringify(items));
    console.debug(ShoppingCartService.TAG, 'updating cart items');
  }
}

