import {Product} from 'app/models/product';

export interface ShoppingCartChangeEvent {
  items: Array<CartItem>
}

export interface CartItem {
  product_id: number;
  quantity: number;
  product?: Product
}
