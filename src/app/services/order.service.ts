import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';

import {ParentService} from './parent';
import {Order} from 'app/models/order';
import {SubOrder} from 'app/models/sub-order';
import {ProductService} from './product';
import {OrderProduct} from 'app/models/order-product';
import {WindowRefService} from './window';

@Injectable({'providedIn': 'root'})
export class OrderService extends ParentService {
  static TAG = 'OrderService';
  static CHECKOUT_DATA_KEY = 'checkoutData';
  private lastCheckoutData = null;
  private window: Window;

  static transformOrderProducts(orderProducts: OrderProduct[]): OrderProduct[] {
    return orderProducts.map(orderProduct => {
      orderProduct.product = ProductService.transformProduct(orderProduct.product);
      return orderProduct;
    })
  }

  constructor(private http: HttpClient,
              windowRef: WindowRefService) {
    super(OrderService.TAG);
    this.window = windowRef.nativeWindow;
    this.api = '/api/v1/orders';
  }

  checkout(payload) {
    return this.http.post('/api/v1/checkout', payload).pipe(catchError(this.handleObsError.bind(this)));
  }

  fetch(id, expand: Array<String> = []): any {
    const expandString = expand ? '?expand=' + expand.join(',') : '';
    return this.http.get<{ order: Order }>(`${this.api}/${id}${expandString}`).pipe(
      map(resp => this.transformOrder(resp.order)),
      catchError(this.handleObsError.bind(this))
    );
  }

  query(queryParams: any): Observable<any> {
    queryParams.user_id = 'me';
    const qpString = this.queryString(queryParams);
    const url = `${this.api}?${qpString}`;
    return this.http.get(url)
      .pipe(
        map((resp: any) => {
          resp.orders = resp.orders.map(order => this.transformOrder(order));
          return resp;
        }),
        catchError(this.handleObsError.bind(this))
      );
  }

  getLastCheckoutData() {
    if (this.lastCheckoutData !== null) {
      return this.lastCheckoutData;
    }
    try {
      const data = window.localStorage.getItem(OrderService.CHECKOUT_DATA_KEY);
      this.lastCheckoutData = JSON.parse(data);
    } catch (e) {
      this.lastCheckoutData = false;
    }
    return this.lastCheckoutData;
  }


  storeLastCheckoutData(data: any) {
    if (data) {
      data = JSON.parse(JSON.stringify(data));
      delete data.password;
      delete data.items;
      this.window.localStorage.setItem(OrderService.CHECKOUT_DATA_KEY, JSON.stringify(data))
    } else {
      this.window.localStorage.removeItem(OrderService.CHECKOUT_DATA_KEY);
      this.lastCheckoutData = false;
    }
  }

  private transformOrder(order: Order) {
    order.items_count = order.orderProducts.length;
    order.discount_total = 0;

    const subOrders = new Map<number, SubOrder>();
    order.subOrders.forEach(subOrder => {
      subOrders.set(subOrder.id, subOrder);
    });

    order.orderProducts = OrderService.transformOrderProducts(order.orderProducts);

    this.calculateQuantities(order, subOrders);

    order.orderProducts = order.orderProducts.map((orderProduct) => {
      const subOrder = subOrders.get(orderProduct.sub_order_id);
      orderProduct.shipped_at = subOrder && subOrder.is_completed ? new Date(subOrder.updated_at) : null;
      return orderProduct;
    });

    return order;
  }

  private calculateQuantities(order: Order, subOrders: Map<number, SubOrder>) {
    [order.ordered_quantity, order.shipped_quantity, order.sold_out_quantity] =
      order.orderProducts.reduce((accumulator: [number, number, number], currentValue) => {
        const subOrder = subOrders.get(currentValue.sub_order_id);
        order.discount_total += currentValue.discount;
        return [
          accumulator[0] + currentValue.quantity,
          accumulator[1] + currentValue.shipped_quantity || 0,
          accumulator[2] + (subOrder && subOrder.is_completed ? currentValue.quantity - currentValue.shipped_quantity : 0)
        ]
          ;
      }, [0, 0, 0]);
  }
}
