import {Inject, Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

import {AUTH_SERVICE_CONFIG} from './config';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {


  private bearerToken: string;
  private communityId: number;

  constructor(@Inject(AUTH_SERVICE_CONFIG) config) {
    this.communityId = config.communityId;
  }

  setAuthHeader(access_token: string) {
    this.bearerToken = `Bearer ${access_token}`;
  }


  clearAuthHeader() {
    this.bearerToken = null;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.bearerToken && !req.headers.has('Authorization')) {
      const authReq = req.clone({headers: req.headers.set('Authorization', this.bearerToken)});
      return next.handle(authReq);
    }
    return next.handle(req);
  }
}
