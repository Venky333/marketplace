import { InjectionToken } from '@angular/core';

export let AUTH_SERVICE_CONFIG = new InjectionToken('auth.service.config');

export interface AuthServiceConfig {
  oauthClientId: string | number;
  communityId: number;
  communityAuthUrl: string;
  communityLogoutUrl: string;
  communitySignUpUrl: string;
}
