import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of as observableOf, Subject} from 'rxjs';
import {map, tap} from 'rxjs/operators';

import {environment} from '../../../environments/environment';
import {AuthToken} from 'app/models';
import {OauthUrls} from 'app/models/oauth-urls';
import {ParentService} from '../parent';
import {AUTH_SERVICE_CONFIG, AuthServiceConfig} from './config';
import {AuthInterceptor} from './auth.interceptor';
import {WindowRefService} from '../window';

@Injectable()
export class AuthService extends ParentService {

  static TOKEN_KEY = 'authToken';
  static TAG = 'AuthService';
  static REMEMBER_ME = 'saveToken';
  static MANAGE_PRODUCTS = 'manageProducts';

  tokenChangeBus = new Subject<AuthToken>();
  switchAccountStarted = new Subject<null>();

  private window: Window;
  private token: AuthToken;
  private config: AuthServiceConfig;
  private apiBaseUrl: string;
  private oauthUrls: OauthUrls;

  constructor(@Inject(AUTH_SERVICE_CONFIG) config,
              private http: HttpClient,
              windowRef: WindowRefService,
              private authInterceptor: AuthInterceptor) {
    super(AuthService.TAG);
    this.window = windowRef.nativeWindow;
    this.config = config;
    this.apiBaseUrl = environment.apiBaseUrl;
    this.retrieveToken();
  }

  communityLogin(code, state) {
    const queryParams = {
      state,
      provider: 'community',
      externalAccessToken: code,
    };
    return this.http.post<AuthToken>(`/api/v1/auth/register-external?`, queryParams)
      .pipe(map(resp => this.startSession(resp)));
  }

  isLoggedIn(): boolean {
    return this.token != null;
  }

  getToken(): AuthToken {
    return this.token;
  }

  getOauthUrl(): Observable<OauthUrls> {
    if (this.oauthUrls) {
      return observableOf(this.oauthUrls);
    }
    return this.http.get<any>(`${this.apiBaseUrl}/api/v1/auth/oauth-url`);
  }

  switchAccount(profile_id) {
    this.switchAccountStarted.next();
    return this.http.post(`${this.apiBaseUrl}/api/v1/auth/switch-profile`, {profile_id})
      .pipe(tap((resp: any) => {
        this.token.token = resp.token;
        this.token.userId = resp.userId;
        this.token.permissions = resp.permissions;
        this.startSession(this.token);
      }));
  }

  userCan(permissions) {
    if (!Array.isArray(permissions)) {
      permissions = [permissions];
    }

    if (!this.token) {
      return false;
    }

    return permissions.some(p => this.token.permissions.includes(p));
  }

  private retrieveToken(): AuthToken {
    const local = true; // this.getRememberMe();
    // let storage = local ? window.localStorage : window.sessionStorage;
    const storage = this.window.localStorage;
    console.debug(AuthService.TAG, 'retrieving token using storage:', local ? 'local' : 'session');
    try {
      const token = storage.getItem(AuthService.TOKEN_KEY);
      this.token = JSON.parse(token);
      return this.token;
    } catch (e) {
      console.error(AuthService.TAG, 'retrieveToken()', e);
      this.token = null;
      return null;
    }
  }

  public startSession(authToken: AuthToken): AuthToken {
    console.debug(AuthService.TAG, 'startSession');
    this.storeToken(authToken);
    this.authInterceptor.setAuthHeader(authToken.token);
    this.tokenChangeBus.next(authToken);
    return authToken;
  }

  private storeToken(token: AuthToken) {
    const local = this.getRememberMe();
    // let storage = local ? window.localStorage : window.sessionStorage;
    const storage = this.window.localStorage;
    token ? storage.setItem(AuthService.TOKEN_KEY, JSON.stringify(token)) : storage.removeItem(AuthService.TOKEN_KEY);
    this.token = token;
    console.debug(AuthService.TAG, 'updating token using storage:', local ? 'local' : 'session');
  }

  public logout() {
    this.authInterceptor.clearAuthHeader();
    this.storeToken(null);
    this.clearStore();
    this.tokenChangeBus.next(null);
  }


  /**
   * @param bool whether user has checked the "remember me" checkbox when last signing in.
   * backed by localStorage
   */
  public setRememberMe(bool: boolean) {
    this.window.localStorage.setItem(AuthService.REMEMBER_ME, JSON.stringify(bool));
  }

  public getRememberMe(): boolean {
    const rememberMe = this.window.localStorage.getItem(AuthService.REMEMBER_ME);
    try {
      return Boolean(JSON.parse(rememberMe));
    } catch (e) {
      this.window.localStorage.removeItem(AuthService.REMEMBER_ME);
      return false;
    }
  }

  private clearStore() {
    this.window.localStorage.removeItem(AuthService.TOKEN_KEY);
    this.window.sessionStorage.removeItem(AuthService.TOKEN_KEY);
  }


}
