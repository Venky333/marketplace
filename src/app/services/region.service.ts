import {Injectable} from '@angular/core';
import {ParentService} from './parent';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, map, shareReplay} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegionService extends ParentService {
  static TAG = 'RegionService';
  private countryCache: Map<string, any> = new Map<string, any>();
  private stateCache: Map<string, Array<any>> = new Map<string, any>();
  private promises: Map<string, Observable<any>> = new Map<string, Observable<any>>();

  constructor(private http: HttpClient) {
    super(RegionService.TAG);
    this.api = environment.accountApiBaseUrl;
  }

  queryCountries(): Observable<any> {
    return this.cacheRegion(`${this.api}/countries`, 'countryCache')
  }

  queryStates(country_id: number): Observable<any> {
    return this.cacheRegion(`${this.api}/countries/${country_id}/states`, 'stateCache')
  }

  getCountry(id): Observable<any> {
    return this.cacheRegion(`${this.api}/countries/${id}`, 'countryCache');
  }

  getState(id): Observable<any> {
    return this.cacheRegion(`${this.api}/states/${id}`, 'countryCache');
  }

  private cacheRegion(url, cache): Observable<any> {
    if (!this[cache].has(url)) {
      if (this[cache].size > 64) {
        this[cache].clear();
      }
      this[cache].set(url, this.http.get(url)
        .pipe(
          map((resp: any) => resp.data),
          catchError(this.handleObsError.bind(this)),
          shareReplay(1)
        ));
    }
    return this[cache].get(url);
  }

}
