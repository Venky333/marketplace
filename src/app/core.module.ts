import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {environment} from 'environments/environment';
import {AUTH_SERVICE_CONFIG, AuthInterceptor, AuthService, AuthServiceConfig} from 'app/services/auth';

const authServiceConfig: AuthServiceConfig = {
  communityId: environment.communityId,
  communityAuthUrl: null,
  oauthClientId: environment.oauthClientId,
  communitySignUpUrl: null,
  communityLogoutUrl: null,
};

// Core Service singletons shared across all other modules
@NgModule({
  imports: [HttpClientModule],
  exports: [],
  providers: [
    AuthService, {provide: AUTH_SERVICE_CONFIG, useValue: authServiceConfig},
    AuthInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: AuthInterceptor,
      multi: true,
    },
  ]
})
export class CoreServicesModule {
}
