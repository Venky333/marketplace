import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {AuthService} from '../../../services/auth';


@Component({
  selector: 'app-switch-account-page',
  template: `
    <div>
      <div class="center-block w-auto-xs p-y-md ">
        <div class="p-a-md">
          <app-loading-placeholder [error]="hasError" [data]="false"></app-loading-placeholder>
        </div>
      </div>
    </div>`
})
export class SwitchAccountPageComponent implements OnInit, OnDestroy {

  hasError = false;

  private subscription: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService) {
  }


  ngOnInit(): void {
    this.subscription = this.route.params.subscribe(params => {
      this.authService.switchAccount(params.id).subscribe(
        () => this.router.navigate(['/']),
        () => this.hasError = true
      );

    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
