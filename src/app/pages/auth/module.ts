import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {SharedModule} from 'app/shared.module'
import {OauthCallbackPageComponent} from './oauth-callback/oauth-callback.component';
import {AuthPageComponent} from './auth-page.component';
import {SwitchAccountPageComponent} from './switch-account/switch-account-page.component';


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: AuthPageComponent,
        children: [
          {
            path: 'oauth-callback',
            component: OauthCallbackPageComponent,
          },
          {
            path: 'switch-account/:id',
            component: SwitchAccountPageComponent,
          }
        ]
      }
    ])
  ],
  declarations: [
    AuthPageComponent, OauthCallbackPageComponent, SwitchAccountPageComponent
  ],
})
export class AuthModule {
}
