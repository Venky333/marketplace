import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'app/services/auth';

@Component({
  selector: 'oauth-callback-page',
  template: `
    <div class="">
      <div class="center-block w-auto-xs p-y-md ">
        <div class="p-a-md">
          <app-loading-placeholder [error]="false" [data]="false"></app-loading-placeholder>
        </div>
      </div>
    </div>`
})
export class OauthCallbackPageComponent implements OnInit {
  private code: string;
  private state: string;


  constructor(private route: Router,
              private authService: AuthService) {
    this.code = route.parseUrl(route.url).queryParams['code'];
    this.state = route.parseUrl(route.url).queryParams['state'];
  }

  ngOnInit() {
    this.authService.communityLogin(this.code, this.state).subscribe(() => {
      this.route.navigate(['/']);
    });
  }

}
