import { InjectionToken } from '@angular/core';

export let AUTH_PAGE_CONFIG = new InjectionToken('auth.page.config');
export interface AuthPageConfig {
  /**
   * absolute route to go after a successful login. For example, "/home".
   */
  loginSuccessRoute: string;
  loginErrorRoute: string;

  /**
   * absolute route to go after a successful sign up. For example, "/home"
   */
  signUpSuccessRoute: string;

  termsRoute: string;
  privacyRoute: string;

  /**
   * routes for the login popover, peek overlay
   */
  loginRoute: string;
  signUpRoute: string;

}
