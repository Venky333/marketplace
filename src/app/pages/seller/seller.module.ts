import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {SharedModule} from '../../shared.module';
import {SellerPageComponent} from './seller-page.component';
import {SellerDashboardPage} from './dashboard/controller';
import {SellerMenubarComponent} from '../../components/seller-menu-bar/seller-menubar.component';
import {SellerMobileMenuComponent} from '../../components/seller-mobile-menu/seller-mobile-menu.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: SellerPageComponent,
        resolve: {
          // user: SellerResolver
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'dashboard'
          },
          {
            path: 'dashboard',
            component: SellerDashboardPage,
          },
          {
            path: 'product',
            loadChildren: 'app/pages/seller/product/product.module#ProductModule'
          },
          {
            path: 'order',
            loadChildren: 'app/pages/seller/order/order.module#OrderModule'
          },
          {
            path: 'report',
            loadChildren: 'app/pages/seller/report/report.module#ReportModule'
          },
          {
            path: 'settings',
            loadChildren: 'app/pages/settings/settings.module#SettingsModule'
          }
        ]
      },
      {
        path: '',
        outlet: 'menubar',
        component: SellerMenubarComponent,
      },
      {
        path: '',
        outlet: 'mobile-menu',
        component: SellerMobileMenuComponent,
      }
    ])
  ],
  declarations: [
    SellerPageComponent,
    SellerDashboardPage,
  ],
})
export class SellerModule {
}
