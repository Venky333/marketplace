import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {environment} from '../../../../environments/environment';
import {ParentService} from 'app/services/parent';
import {SubOrder} from 'app/models/sub-order';
import {ListingResponse} from 'app/models/resources-response';
import {OrderProduct} from 'app/models/order-product';
import {OrderService} from '../../../services/order.service';

@Injectable({providedIn: 'root'})
export class SellerOrderService extends ParentService {
  static TAG = 'SellerOrderService';
  static STATE_NEW = 1;
  static STATE_OVERDUE = 2;
  static STATE_SHIPPED = 3;
  static STATE_RETURN = 4;
  static STATE_SOLD_OUT = 5;
  orderListingUpdated = new Subject<any>();

  static transformOrder(subOrder: SubOrder) {
    if (subOrder.orderProducts) {
      subOrder.orderProducts = OrderService.transformOrderProducts(subOrder.orderProducts);
    }
    return subOrder;
  }

  constructor(private http: HttpClient) {
    super(SellerOrderService.TAG);
    this.api = `${environment.apiBaseUrl}/api/v1/seller/orders`;
  }

  query(queryParams: any): Observable<ListingResponse<SubOrder>> {
    const url = `${this.api}?${this.queryString(queryParams)}`;
    return this.http.get<ListingResponse<SubOrder>>(url).pipe(
      map((resp: any) => {
        resp.subOrders = resp.subOrders.map(SellerOrderService.transformOrder);
        return resp;
      }),
      catchError(this.handleObsError.bind(this))
    );
  }

  fetch(id: number): Observable<SubOrder> {
    return this.http.get<{ subOrder: SubOrder }>(`${this.api}/${id}?expand=mainOrder,orderProducts,orderProducts.product`)
      .pipe(map(resp => SellerOrderService.transformOrder(resp.subOrder)));
  }

  update(id, data): Observable<SubOrder> {
    return this.http.put<{ subOrder: SubOrder }>(`${this.api}/${id}`, data)
      .pipe(
        map(resp => resp.subOrder),
        tap(() => this.orderListingUpdated.next())
      )
      ;
  }

  getStatistics() {
    return this.http.get<any>(`${this.api}/statistics`)
      .pipe(map(resp => resp.statistics));
  }

  calcTotals(items: OrderProduct[]) {
    let total = 0, itemsCount = 0;
    if (items) {
      items.map(item => {
        total += (item.quantity * (item.total));
        itemsCount += item.quantity;
      });
    }
    return {total, itemsCount};
  }


}
