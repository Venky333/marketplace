import {Component, OnDestroy} from '@angular/core';
import {SellerOrderService} from './seller-order.service';
import {Subscription} from 'rxjs';

@Component({
  templateUrl: './order-page.component.html'
})
export class OrderPageComponent implements OnDestroy {
  STATE_NEW = SellerOrderService.STATE_NEW;
  STATE_OVERDUE = SellerOrderService.STATE_OVERDUE;
  STATE_SHIPPED = SellerOrderService.STATE_SHIPPED;
  STATE_RETURN = SellerOrderService.STATE_RETURN;
  STATE_SOLD_OUT = SellerOrderService.STATE_SOLD_OUT;

  statistics: any = {};
  private sub: Subscription;

  constructor(private orderService: SellerOrderService) {
    this.getStatistics();
    this.sub = this.orderService.orderListingUpdated.subscribe(() => {
      this.getStatistics();
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private getStatistics() {
    this.orderService.getStatistics().subscribe(data => this.statistics = data);
  }
}
