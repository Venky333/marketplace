import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {SubOrder} from 'app/models/sub-order';
import {RegionService} from 'app/services/region.service';
import {SellerOrderService} from '../seller-order.service';
import {OrderDetailsModalComponent} from 'app/components/order-details-modal/order-details-modal.component';
import {AccountService} from '../../../../services/account/account.service';

@Component({
  templateUrl: './order-listing-page.component.html',
  styleUrls: ['./order-listing-page.component.scss']
})
export class OrderListingPageComponent implements OnInit, OnDestroy {
  static TAG = 'OrderListingPageComponent';

  queryCall: Subscription;
  page = 1;
  pageSize = 10;
  sortBy = 'created_at';
  sortOrder = 'desc';
  hasError: boolean;
  totalCount: number;
  pageCount: number;
  modelsName = 'subOrders';
  models: Array<SubOrder> = [];
  showLoadMoreButton: boolean;
  state: number;
  title = 'All Orders';
  @ViewChild('rightModal') rightModal;
  private subs: Array<Subscription>;
  private buyer_id: string;

  constructor(private orderService: SellerOrderService,
              private router: Router,
              private regionService: RegionService,
              private accountService: AccountService,
              private modalService: NgbModal,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.subs = [
      this.route.queryParams.subscribe((params: Params) => {
        this.state = params.state || null;
        this.buyer_id = params.buyer_id || null;
        this.search(Number(params.page || 1));
        this.buildTitle();
      }),
    ];
  }


  showOrderDetail(order) {
    const modalRef = this.modalService.open(OrderDetailsModalComponent, {windowClass: 'modal-right'});
    modalRef.componentInstance.setOrder(order);
    modalRef.result
      .then(() => this.search())
      .catch(() => this.search());
  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }
  }

  trackByOrderId(index, item) {
    if (!item) {
      return null;
    }
    return item.id;
  }

  toggleSort(property) {
    if (!property) {
      return;
    }

    if (this.sortBy === property) {
      switch (this.sortOrder) {
        case 'asc':
          this.sortOrder = 'desc';
          break;
        case 'desc':
          this.sortOrder = '';
          this.sortBy = '';
          break;
        default:
          this.sortOrder = 'asc';
      }
    } else {
      this.sortOrder = 'asc';
      this.sortBy = property;
    }

    this.search();
  }

  getNextPage() {
    this.showLoadMoreButton = false;
    this.search(this.page + 1, true);
  }

  private search(page: number = 1, loadMore = false) {
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }

    const queryParams = this.getQueryParams(page);
    console.debug(OrderListingPageComponent.TAG, 'search', queryParams);

    this.queryCall = this.orderService.query(queryParams).subscribe((results: any) => {
      this.page = page;
      const data = this.prepare(results[this.modelsName]);
      this.models = (loadMore) ? this.models.concat(data) : data;
      this.totalCount = results.meta.total;
      this.pageCount = results.meta.last_page;
      this.hasError = false;
      this.showLoadMoreButton = page < this.pageCount;
    }, () => {
      this.hasError = true;
      this.models = [];
    });
  }

  private getQueryParams(pageNumber) {
    const q: any = {
      'per-page': this.pageSize,
      page: pageNumber,
      expand: 'mainOrder,orderProducts,orderProducts.products'
    };
    if (this.sortBy && this.sortOrder) {
      q.sort = (this.sortOrder === 'asc' ? '' : '-') + this.sortBy;
    }
    if (this.state) {
      q.state = this.state;
    }
    if (this.buyer_id) {
      q.buyer_id = this.buyer_id;
    }
    return q;
  }

  private prepare(result) {
    return result.map(order => {
      order.shipping_country$ = this.getCountry(order);
      return order;
    })
  }

  private getCountry(order) {
    return this.regionService.getCountry(order.mainOrder.shipping_country_id).pipe(map(c => c.name));
  }

  private buildTitle() {
    if (this.buyer_id) {
      this.accountService.fetchProfile(parseInt(this.buyer_id, 10))
        .subscribe(user => this.title = `Orders - ${user.public_identity}`)
    } else {
      this.title = 'All Orders';
    }
  }
}

