import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NgbDropdownModule, NgbModalModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {SharedModule} from 'app/shared.module';

import {OrderPageComponent} from './order-page.component';
import {OrderListingPageComponent} from './order-listing-page/order-listing-page.component';


@NgModule({
  declarations: [
    OrderPageComponent,
    OrderListingPageComponent,
  ],
  imports: [
    SharedModule,
    NgbModalModule,
    NgbTooltipModule,
    NgbDropdownModule,
    RouterModule.forChild([
      {
        path: '',
        component: OrderPageComponent,
        children: [
          {
            path: '',
            component: OrderListingPageComponent
          }
        ]
      }
    ])
  ],
  entryComponents: []
})

export class OrderModule {

}
