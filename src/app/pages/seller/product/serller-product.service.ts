import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable, Subject} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {ParentService} from 'app/services/parent';
import {ProductService} from 'app/services/product/product.service';
import {ProductLabel} from 'app/models/product-label';
import {Product} from 'app/models';
import {environment} from '../../../../environments/environment';
import {ListingResponse} from '../../../models/resources-response';

@Injectable()
export class SellerProductService extends ParentService {
  static TAG = 'SellerProductService';

  static MAX_IMAGES_COUNT = 5;

  productListingUpdated = new Subject<any>();

  static makeItem(): any {

    return {
      id: null,
      status: ProductService.PRODUCT_STATUS_DRAFT,
      name: null,
      model: null,
      category_id: null,
      sku: null,
      quantity: 1,
      price: null,
      tags: [],
      images: [],
      is_special: false,
      is_clearance: false
    };
  }

  constructor(private http: HttpClient) {
    super(SellerProductService.TAG);
    this.api = `${environment.apiBaseUrl}/api/v1/seller/products`;
  }

  create(product): Observable<any> {
    return this.http.post(`${this.api}`, product).pipe(
      tap(() => this.productListingUpdated.next()),
      catchError(this.handleObsError.bind(this))
    );
  }

  delete(product): Observable<any> {
    return this.http.delete(`${this.api}/${product.id}`).pipe(
      tap(() => this.productListingUpdated.next()),
      catchError(this.handleObsError.bind(this))
    );
  }

  update(id, data): Observable<any> {
    data.special_label = data.is_special ? ProductLabel.SPECIAL : (data.is_clearance ? ProductLabel.CLEARANCE : null);
    return this.http.put(`${this.api}/${id}`, data)
      .pipe(
        tap(() => this.productListingUpdated.next()),
        catchError(this.handleObsError.bind(this))
      );
  }

  uploadImage(file: File): Observable<any> {
    const url = `/api/v1/seller/products/images`;
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post(url, formData).pipe(catchError(this.handleObsError.bind(this)));
  }

  query(queryParams: any): Observable<ListingResponse<Product>> {
    const qpString = this.queryString(queryParams);
    return this.http.get(`${this.api}?${qpString}`).pipe(
      map((resp: any) => {
        resp.products = resp.products.map(p => ProductService.transformProduct(p));
        return resp;
      }),
      catchError(this.handleObsError.bind(this))
    );

  }

  fetch(id: string, expand: Array<String> = []): Promise<any> {
    const expandString = expand ? '?expand=' + expand.join(',') : '';
    return this.http.get(`${this.api}/${id}${expandString}`)
      .pipe(map((resp: any) => ProductService.transformProduct(resp.product)))
      .toPromise()
      .catch(this.handlePromiseError.bind(this));
  }

  suggest(query: string, type: string): Observable<Array<string>> {
    if (query && query.length) {
      const qpString = `q=${query}`;
      const url = `/api/v1/suggest/${type}?${qpString}`;
      return this.http.get<Array<string>>(url);
    }
    return EMPTY;
  }

  fetchStatistics() {
    return this.http.get(`${this.api}/statistics`)
      .pipe(map((resp: any) => {
        const statistics = resp.statistics;
        statistics.draft = resp.statistics.draft + resp.statistics.posted_community;
        delete statistics.posted_community;
        return statistics;
      }));
  }

  updateQuantity(id: number, quantity: number) {
    return this.http.post(`${this.api}/${id}/quantity`, {quantity}).pipe(
      tap(() => this.productListingUpdated.next()),
      catchError(this.handleObsError.bind(this))
    );
  }
}

