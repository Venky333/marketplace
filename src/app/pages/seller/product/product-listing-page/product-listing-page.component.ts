import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

import {ProductService} from 'app/services/product';
import {WindowRefService} from 'app/services/window';
import {Product} from 'app/models';
import {SellerProductService} from '../serller-product.service';

@Component({
  selector: 'app-product-listing-page',
  templateUrl: './product-listing-page.component.html',
  styleUrls: ['./product-listing-page.component.scss']
})
export class ProductListingPageComponent implements OnInit, OnDestroy {
  static TAG = 'ProductListingPageComponent';

  queryCall: Subscription;
  sortBy = 'created_at';
  sortOrder = 'desc';
  searchTerm = '';
  searchedTerm = '';
  page = 1;
  pageSize = 10;
  hasError: boolean;
  totalCount: number;
  pageCount: number;
  modelsName = 'products';
  models: Array<Product> = [];
  statusMap = {};
  statusMapIndexes = [];
  labelType: string;
  status: string;
  is_sold: string;
  loading = false;
  sortColumns: Array<any> = [
    {
      label: 'Modified',
      property: 'updated_at',
    }, {
      label: 'Item',
      property: '',
    }, {
      label: 'Description',
      property: 'name',
    },
    {
      label: 'In stock',
      property: 'quantity',
    },
    {
      label: 'Status',
      property: '',
    },
  ];

  showLoadMoreButton: boolean;
  private subs: Array<Subscription>;

  constructor(private productService: SellerProductService,
              private prService: ProductService,
              private windowService: WindowRefService,
              private toastr: ToastrService,
              private router: Router,
              private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.subs = [
      this.route.queryParams.subscribe((params: Params) => {
        this.searchTerm = params.searchTerm || '';
        this.labelType = params.labelType || null;
        this.status = params.status || null;
        this.is_sold = params.is_sold || null;
        this.search(Number(params.page || 1));
      }),
    ];
    this.statusMap = this.prService.statusLabels;
    this.statusMapIndexes = Object.keys(this.statusMap);

  }


  toggleSort(property: string) {
    if (!property) {
      return;
    }

    if (this.sortBy === property) {
      switch (this.sortOrder) {
        case 'asc':
          this.sortOrder = 'desc';
          break;
        case 'desc':
          this.sortOrder = '';
          this.sortBy = '';
          break;
        default:
          this.sortOrder = 'asc';
      }
    } else {
      this.sortOrder = 'asc';
      this.sortBy = property;
    }

    this.search();
  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }
  }

  updateQuantity(event: KeyboardEvent, index, newValue) {
    const item = {...this.models[index]};
    if (
      (!event || (event.keyCode === 10 || event.keyCode === 13))
      && (/^\d+$/.test(newValue) && newValue >= 0 && item.quantity !== parseInt(newValue, 10))
    ) {
      if (event) {
        event.preventDefault();
      }
      this.loading = true;
      item.quantity = parseInt(newValue, 10);
      this.productService.updateQuantity(item.id, item.quantity)
        .pipe(
          finalize(() => this.loading = false)
        )
        .subscribe(
          () => {
            this.toastr.success('Product stock quantity updated successfully');
            this.models[index] = item;
          },
          (fail) => {
            this.toastr.error('Error occurred while trying update stock quantity ' + fail.message)
          }
        )
      ;
    }
  }

  getNextPage() {
    this.showLoadMoreButton = false;
    this.search(this.page + 1, true);
  }


  private search(page: number = 1, loadMore = false) {
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }

    const queryParams = this.getQueryParams(page);
    console.debug(ProductListingPageComponent.TAG, 'search', queryParams);

    this.queryCall = this.productService.query(queryParams).subscribe((results: any) => {
      this.searchedTerm = queryParams.searchTerm || '';
      this.page = page;
      this.pageSize = queryParams.pageSize || this.pageSize;
      this.models = (loadMore) ? this.models.concat(results[this.modelsName]) : results[this.modelsName];
      this.totalCount = results.meta.total;
      this.pageCount = results.meta.last_page;
      this.showLoadMoreButton = page < this.pageCount;
      this.hasError = false;
    }, () => {
      this.hasError = true;
      this.models = [];
    });
  }

  private getQueryParams(pageNumber) {
    const q: any = {
      'per-page': this.pageSize,
      page: pageNumber,
      expand: 'primaryImage'
    };
    if (this.searchTerm && this.searchTerm.trim().length) {
      q.searchTerm = this.searchTerm.trim();
    }
    if (this.sortBy && this.sortOrder) {
      q.sort = (this.sortOrder === 'asc' ? '' : '-') + this.sortBy;
    }
    if (this.status) {
      q.status = [this.status];
    }
    if (this.labelType) {
      switch (this.labelType) {
        case 'clearance':
          q.is_clearance = 1;
          break;
        case 'special':
          q.is_special = 1;
          break;
      }
    }

    if (this.is_sold) {
      q.is_sold = 1;
    }
    return q;
  }
}
