import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'product-image-cropper',
  templateUrl: './product-image-cropper.component.html'
})
export class ProductImageCropperComponent {

  cropperImage: Blob;
  imageAspectRatio = 1;

  constructor(public activeModal: NgbActiveModal) {
  }
}
