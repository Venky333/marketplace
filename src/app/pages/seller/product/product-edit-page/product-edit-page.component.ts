import {Component, NgZone, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {Observable, of as observableOf, throwError as observableThrowError} from 'rxjs';
import {catchError, filter, flatMap, map, mergeMap, tap} from 'rxjs/operators';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import * as cloneDeep from 'lodash.clonedeep';

import {Category} from 'app/services/content';
import {ProductService} from 'app/services/product';
import {FormHelper, getErrorMsg} from 'app/helpers/utils';
import {Product} from 'app/models';
import {SellerProductService} from '../serller-product.service';
import {RULES} from './validation-rules';
import {ProductImageCropperComponent} from './components/product-image-cropper.component';

const IMAGE_FILL_COLOR = '#ffffff';

@Component({
  selector: 'app-product-edit-page',
  templateUrl: './product-edit-page.component.html',
  styleUrls: ['./product-edit-page.component.scss']
})
export class ProductEditPageComponent implements OnInit {

  @ViewChild('previewModal') previewModal: TemplateRef<any>;
  previewModalRef: NgbModalRef;
  @ViewChild('confirmationModal') confirmationModal: TemplateRef<any>;
  confirmationModalRef: NgbModalRef;
  @ViewChild('cropperModal') cropperModal: TemplateRef<any>;
  cropperModalRef: NgbModalRef;
  extraFieldsRequired = false;
  ngForm: FormGroup;
  confirmAction = null;
  confirmMessage: string;
  STATUS_DRAFT: number;
  STATUS_PUBLISHED = ProductService.PRODUCT_STATUS_PUBLISHED;
  STATUS_WITHDRAWN = ProductService.PRODUCT_STATUS_WITHDRAWN;
  MAX_IMAGES_COUNT = SellerProductService.MAX_IMAGES_COUNT;
  currentImageIndex = 0;
  categories: Array<Category>;
  tag: string;
  category: any = [];
  product: Product;
  isNew: boolean;
  noImages: boolean;
  showingPreview = false;
  processing = false;
  processInfo = '';
  processErrorMsg: string = null;
  unhandledValidationErrors = [];
  tagLoading = false;
  canPublish = false;
  modelLoading = false;
  maxPublishItems;

  quillEditorModules = {
    toolbar: [
      ['bold', 'italic', 'underline'],        // toggled buttons
      ['blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}],
      [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
    ]
  };


  constructor(private route: ActivatedRoute,
              protected router: Router,
              private zone: NgZone,
              private fb: FormBuilder,
              private toastr: ToastrService,
              private modalService: NgbModal,
              private productService: SellerProductService) {
    this.ngForm = this.fb.group({
      status: [ProductService.PRODUCT_STATUS_DRAFT, [Validators.required]],
      name: null,
      model: null,
      brand: null,
      category_id: null,
      description: null,
      price: null,
      requires_shipping: null,
      discount_price: null,
      unit_of_measure: null,
      quantity: null,
      minimum_order_quantity: null,
      sku: null,
      is_special: null,
      is_clearance: null,
      subtract: null,
      weight: null,
      length: null,
      height: null,
      width: null,
      shipping_weight: null,
      shipping_length: null,
      shipping_height: null,
      shipping_width: null,
      tagSearch: null
    });
    this.ngForm.get('is_special').valueChanges.subscribe(v => {
      if (v === true) {
        this.ngForm.get('is_clearance').setValue(false);
      }
    });
    this.ngForm.get('is_clearance').valueChanges.subscribe(v => {
      if (v === true) {
        this.ngForm.get('is_special').setValue(false);
      }
    });

    this.ngForm.get('requires_shipping').valueChanges.subscribe(v => {
      this.updateShippingDimensionRules(v);
    });

    this.ngForm.get('status').valueChanges.subscribe(status => {
      this.updateRules(status);

    });
  }

  ngOnInit() {
    this.initItem();
    this.initSuggestSource();
  }

  showPreview() {
    this.product = Object.assign({}, this.product, this.ngForm.getRawValue());
    this.previewModalRef = this.modalService.open(this.previewModal, {backdrop: 'static', size: 'lg'});
    setTimeout(() => this.showingPreview = true, 600);
  }

  hidePreview() {
    this.showingPreview = false;
    this.previewModalRef.close();
  }

  handleImageSelect(input: HTMLInputElement) {
    const file: File = input.files[0];

    // Create an img element and add the image file data to it
    const img = document.createElement('img');
    img.src = window.URL.createObjectURL(file);
    let reader: any;
    reader = new FileReader();

    // Add an event listener to deal with the file when the reader is complete
    reader.addEventListener('load', (event) => {
      // Get the event.target.result from the reader (base64 of the image)
      img.src = event.target.result;

      const type = img.src.split(';')[0];
      input.value = null;
      if (type !== 'data:image/jpeg' && type !== 'data:image/png') {
        return;
      }

      this.cropperModalRef = this.modalService.open(ProductImageCropperComponent, {
        windowClass: 'image-cropper',
        backdrop: 'static',
        keyboard: false,
      });
      this.cropperModalRef.componentInstance.cropperImage = img.src;
      this.cropperModalRef.result.then((result) => {
        if (result) {
          this.addImage(result);
        }
      });
      this.resetImageError();
    }, false);

    reader.readAsDataURL(file);
  }


  addImage(angularCropper) {
    const canvas: HTMLCanvasElement = angularCropper.cropper.getCroppedCanvas({
      fillColor: IMAGE_FILL_COLOR
    });
    canvas.toBlob((blob) => {
      const file = new File([blob], 'image.jpeg');
      const image: any = {url: canvas.toDataURL('image/jpeg'), file};
      this.product.images.push(image);
    }, 'image/jpeg');

  }

  removeImage(index) {
    this.product.images.splice(index, 1);
    this.resetImageError();
    this.currentImageIndex = 0;
    this.noImages = this.product.images.length === 0;
  }

  handleConfirmAction() {
    this[this.confirmAction]();
    this.resetConfirm();
  }

  cancelConfirmation() {
    this.resetConfirm();
  }

  confirmRemove() {
    setTimeout(() => {
      this.confirmMessage = 'Confirm remove?';
      this.confirmAction = 'remove';
      this.confirmationModalRef = this.modalService.open(this.confirmationModal,
        {
          backdrop: 'static',
          keyboard: false,
          windowClass: 'confirm-modal',
        });

    });
  }

  remove() {
    this.processing = true;
    this.productService.delete(this.product)
      .subscribe(
        () => this.router.navigate(['/seller/product'])
          .then(() => {
            const message = `Product <strong>${this.product.name}</strong> has been removed successfully`;
            this.toastr.success(message, null, {enableHtml: true})
          }),
        fail => {
          this.processing = false;
          this.processErrorMsg = fail.message;

        }
      );
  }

  cancel() {
    this.router.navigate(['/seller/product']);
  }

  save() {
    if (this.ngForm.invalid || ((this.noImages || !this.product.tags.length) && this.extraFieldsRequired)) {
      FormHelper.markAsTouched(this.ngForm);
      return;
    }
    this.resetErrors();
    this.processing = true;
    let obs = observableOf(null);
    this.processInfo = `Uploading images...`;
    this.product.images.forEach((image, index) => {
      if (!image.path) {
        obs = obs.pipe(
          flatMap(() => this.uploadImage(image).pipe(
            filter((resp: any) => resp.path),
            tap(resp => this.product.images[index] = resp))
          )
        );
      }
    });

    obs.pipe(
      flatMap(() => {
        // makes a copy of card object instead of operating
        const item = Object.assign({}, this.product, this.ngForm.getRawValue());
        this.setFalsyValues(item);
        this.processInfo = 'Preparing product...';
        item.images = this.product.images.filter(image => image.path).map(image => image.path);
        return (item.id ? this.productService.update(item.id, item) : this.productService.create(item))
          .pipe(
            tap(() => this.processInfo = 'Completing submission...'),
            catchError((fail: HttpErrorResponse) => {
              if (fail.status === 422) {
                return observableThrowError({'saveError': true, response: fail});
              }
              return observableThrowError('Error loading item: ' + getErrorMsg(fail));
            })
          );
      }),
    ).subscribe(
      (resp: any) => {
        setTimeout(() => {
          const product = resp.product;
          this.product.status = product.status;
          this.processing = false;
          this.router.navigate(['/seller/product/edit', 't' + (new Date()).getTime(), product.id])
            .then(() => {
              const message = `Product <strong>${product.name}</strong> has been ${this.isNew ? 'added' : 'updated'} successfully`;
              this.toastr.success(message, null, {enableHtml: true})
            });
        }, 500);
      },
      (fail) => {
        this.processing = false;
        if (fail.saveError) {
          fail.response.error.errors.forEach((e) => {
            if (this.ngForm.controls[e.field]) {
              this.ngForm.controls[e.field].setErrors({server: e.message});
            } else {
              this.unhandledValidationErrors.push(e.message);
            }
          });
        } else if (fail) {
          this.processErrorMsg = fail;
        }
      }
    );
  }


  addTag(tag: string) {
    if (tag) {
      tag = tag.toLowerCase().trim();
      if (tag !== '' && !this.product.tags.some(t => t === tag)) {
        this.product.tags.push(tag);
      }
      this.ngForm.get('tagSearch').setValue(null, {emitEvent: false});
    }

  }

  preventEnterKeyDefault(event: KeyboardEvent) {
    if ((event.keyCode === 10 || event.keyCode === 13)) {
      event.preventDefault();
      this.addTag(this.ngForm.get('tagSearch').value);
    }
  }

  removeTag(tag) {
    const index = this.product.tags.indexOf(tag);
    this.product.tags.splice(index, 1);
  }

  modelSearch = ($text: Observable<string>) => {
    return $text.pipe(
      mergeMap((token: string) => this.productService.suggest(token, 'model'))
    );
  }

  tagSearch = ($text: Observable<string>) => {
    return $text.pipe(
      mergeMap((token: string) => this.productService.suggest(token, 'tag'))
    );
  };

  private initItem() {
    this.route.data.forEach((data: any) => {
      this.product = data.results.item;
      this.extraFieldsRequired = this.product.status === this.STATUS_PUBLISHED
        || this.product.status === ProductService.PRODUCT_STATUS_PUBLISHED_COMMUNITY;
      this.STATUS_DRAFT = this.product.status === ProductService.PRODUCT_STATUS_PUBLISHED_COMMUNITY
        ? ProductService.PRODUCT_STATUS_PUBLISHED_COMMUNITY
        : ProductService.PRODUCT_STATUS_DRAFT;
      this.ngForm.reset(this.product, {onlySelf: false});
      this.canPublish = (data.results.publishQuota.max_value > data.results.publishQuota.current_value)
        || this.product.status === this.STATUS_PUBLISHED;
      this.maxPublishItems = data.results.publishQuota.max_value;
      this.categories = data.results.categories;

    });
    this.isNew = !this.product.id;
    this.noImages = this.isNew || this.product.images.length === 0;
  }


  private initSuggestSource() {

  }

  private uploadImage(image) {
    return this.productService.uploadImage(image.file)
      .pipe(
        map(resp => resp.image),
        catchError(fail => observableThrowError(`Error uploading image : ` + getErrorMsg(fail)))
      );
  }

  private resetErrors() {
    this.processErrorMsg = null;
    this.unhandledValidationErrors = [];
  }

  private resetConfirm() {
    this.confirmMessage = '';
    this.confirmationModalRef.close();
    this.confirmAction = null;
  }

  private resetImageError() {
    this.noImages = false;
    this.processErrorMsg = null;
  }

  private setRules(rules: any) {
    Object.keys(rules).forEach(f => {
      this.ngForm.get(f).setValidators(rules[f]);
      this.ngForm.get(f).updateValueAndValidity();
    });

  }

  private updateRules(status: any) {
    console.log('updating rules');
    const rules = cloneDeep(RULES);
    let mandatoryFields: string[] = [];
    status = parseInt(status, 10);
    this.extraFieldsRequired = false;
    if (status === ProductService.PRODUCT_STATUS_PUBLISHED) {
      this.extraFieldsRequired = true;
      mandatoryFields = ['category_id', 'description', 'price', 'unit_of_measure', 'quantity'];
    } else if (status === ProductService.PRODUCT_STATUS_PUBLISHED_COMMUNITY) {
      this.extraFieldsRequired = true;
      mandatoryFields = ['description', 'category_id', 'unit_of_measure'];
      if (!this.product.price_on_application) {
        mandatoryFields.push('price');
      }
    }
    mandatoryFields.forEach(field => {
      rules[field].push(Validators.required)
    });
    this.setRules(rules);
  }

  private updateShippingDimensionRules(value: any) {
    let rules: any = cloneDeep(RULES);
    rules = {
      shipping_weight: rules.shipping_weight,
      shipping_length: rules.shipping_length,
      shipping_height: rules.shipping_height,
      shipping_width: rules.shipping_width,
    };
    const fields = ['shipping_weight', 'shipping_length', 'shipping_height', 'shipping_width'];
    if (value) {
      if (this.ngForm.get('status').value === ProductService.PRODUCT_STATUS_PUBLISHED) {
        fields.forEach(field => rules[field].push(Validators.required))
      }
      fields.forEach(field => this.ngForm.get(field).enable());
      this.setRules(rules);
    } else {
      fields.forEach(field => {
        this.ngForm.get(field).reset(null);
        this.ngForm.get(field).disable();
        this.ngForm.get(field).updateValueAndValidity();
      })
    }

  }

  private setFalsyValues(item: any) {
    ['requires_shipping', 'subtract', 'price_on_application'].forEach(field => {
      if (item[field] === null) {
        item[field] = false;
      }
    })
  }
}
