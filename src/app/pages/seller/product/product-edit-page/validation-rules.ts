import {Validators} from '@angular/forms';

import {RegexUtil} from 'app/helpers/regex-util';
import {checkDiscountSmallThanPrice, FormHelper} from 'app/helpers/utils';

const NUMBER_PATTERN = RegexUtil.NUMBER_PATTERN;

export const RULES = {
  name: [Validators.required, Validators.minLength(5)],
  model: [Validators.required, Validators.minLength(2)],
  brand: [],
  category_id: [],
  description: [Validators.minLength(5)],
  price: [Validators.pattern(new RegExp(RegexUtil.NON_ZERO_POSITIVE_NUMBERS))],
  discount_price: [Validators.pattern(new RegExp(RegexUtil.NON_ZERO_POSITIVE_NUMBERS)), checkDiscountSmallThanPrice],
  unit_of_measure: [],
  quantity: [Validators.min(0), Validators.pattern(NUMBER_PATTERN)],
  minimum_order_quantity: [FormHelper.biggerThan(0), Validators.pattern(NUMBER_PATTERN)],
  sku: [Validators.minLength(3)],
  is_special: [],
  is_clearance: [],
  subtract: [],
  weight: [FormHelper.biggerThan(0), FormHelper.number],
  length: [FormHelper.biggerThan(0), FormHelper.number],
  height: [FormHelper.biggerThan(0), FormHelper.number],
  width: [FormHelper.biggerThan(0), FormHelper.number],
  requires_shipping: [],
  shipping_weight: [FormHelper.biggerThan(0), FormHelper.number],
  shipping_length: [FormHelper.biggerThan(0), FormHelper.number],
  shipping_height: [FormHelper.biggerThan(0), FormHelper.number],
  shipping_width: [FormHelper.biggerThan(0), FormHelper.number],
};

export const RULES2 = {
  name: [Validators.required, Validators.minLength(3)],
  model: [Validators.required, Validators.minLength(2)],
  brand: null,
  category_id: null,
  description: null,
  price: [Validators.pattern(new RegExp(RegexUtil.NON_ZERO_POSITIVE_NUMBERS))],
  discount_price: [Validators.pattern(new RegExp(RegexUtil.NON_ZERO_POSITIVE_NUMBERS)), checkDiscountSmallThanPrice],
  requires_shipping: null,
  unit_of_measure: null,
  quantity: [Validators.min(0), Validators.pattern(NUMBER_PATTERN)],
  minimum_order_quantity: [FormHelper.biggerThan(0), Validators.pattern(NUMBER_PATTERN)],
  sku: null,
  is_special: null,
  is_clearance: null,
  subtract: null,
  weight: [FormHelper.biggerThan(0), FormHelper.number],
  length: [FormHelper.biggerThan(0), FormHelper.number],
  height: [FormHelper.biggerThan(0), FormHelper.number],
  width: [FormHelper.biggerThan(0), FormHelper.number],
  shipping_weight: [FormHelper.biggerThan(0), FormHelper.number],
  shipping_length: [FormHelper.biggerThan(0), FormHelper.number],
  shipping_height: [FormHelper.biggerThan(0), FormHelper.number],
  shipping_width: [FormHelper.biggerThan(0), FormHelper.number],
};
