import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';

import {CategoryService} from 'app/services/content';
import {ErrorRerouteMixin} from 'app/helpers/error-reroute-mixin';
import {AccountService} from 'app/services/account/account.service';
import {SellerProductService} from '../serller-product.service';

@Injectable()
export class ProductEditResolver implements Resolve<any> {

  constructor(protected router: Router, // needed error route mixin
              private contentService: CategoryService,
              private accountService: AccountService,
              private productService: SellerProductService) {
  }

  resolve(route: ActivatedRouteSnapshot): any {
    const id = route.params['id'];

    return Promise.all([
      id === undefined
        ? Promise.resolve(SellerProductService.makeItem())
        : this.productService.fetch(id),
      this.contentService.query().toPromise(),
      this.accountService.fetchQuota().toPromise()
    ]).then(
      (results: Array<any>) => {
        const publishQuota = results[2].find((q) => q.quota === AccountService.PUBLISHED_PRODUCT_QUOTA);
        return {item: results[0], categories: results[1], publishQuota};
      },
      (fail) => ErrorRerouteMixin.handleErrorResp.call(this, fail)
    );

  }
}
