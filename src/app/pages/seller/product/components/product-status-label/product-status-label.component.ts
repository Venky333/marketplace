import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {ProductService} from 'app/services/product';


@Component({
  selector: 'app-product-status-label',
  template: `
    <ng-container [ngSwitch]="status">
      <div *ngSwitchCase="STATUS_PUBLISHED" class="product-status-label product-status-label--published">{{statusLabel}}</div>
      <div *ngSwitchCase="STATUS_DRAFT" class="product-status-label product-status-label--draft">{{statusLabel}}</div>
      <div *ngSwitchCase="STATUS_PUBLISHED_COMMUNITY" class="product-status-label product-status-label--draft">{{statusLabel}}</div>
      <div *ngSwitchCase="STATUS_WITHDRAWN" class="product-status-label product-status-label--withdrawn">{{statusLabel}}</div>
      <div *ngSwitchCase="STATUS_PENDING" class="product-status-label product-status-label--pending">{{statusLabel}}</div>
      <div *ngSwitchCase="STATUS_SOLD" class="product-status-label product-status-label--sold">{{statusLabel}}</div>
      <div *ngSwitchDefault class="product-status-label product-status-label--unknown">{{statusLabel}}</div>
    </ng-container>
  `,
  styleUrls: ['./product-status-label.component.scss']
})

export class ProductStatusLabelComponent implements OnChanges {

  statusLabel: any;
  status: any;

  STATUS_PUBLISHED = ProductService.PRODUCT_STATUS_PUBLISHED;
  STATUS_WITHDRAWN = ProductService.PRODUCT_STATUS_WITHDRAWN;
  STATUS_PENDING = ProductService.PRODUCT_STATUS_PENDING;
  STATUS_DRAFT = ProductService.PRODUCT_STATUS_DRAFT;
  STATUS_PUBLISHED_COMMUNITY = ProductService.PRODUCT_STATUS_PUBLISHED_COMMUNITY;
  STATUS_SOLD = 'sold';

  private statusLabels;

  constructor(productService: ProductService) {
    this.statusLabels = productService.statusLabels;
  }

  @Input() product;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('product') && this.product) {
      this.setLabel();
    }
  }

  private setLabel() {
    if (this.product.status === this.STATUS_PUBLISHED && this.product.quantity <= 0) {
      this.statusLabel = 'SOLD OUT';
      this.status = this.STATUS_SOLD;

    } else {
      this.statusLabel = this.statusLabels.hasOwnProperty(this.product.status) ? this.statusLabels[this.product.status] : 'Unknown';
      this.status = this.product.status;
    }
  }


}
