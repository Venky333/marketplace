import {Component, OnDestroy} from '@angular/core';
import {SellerProductService} from './serller-product.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-seller-product-page',
  templateUrl: './seller-products-page.component.html'
})
export class SellerProductsPageComponent implements OnDestroy {
  statistics: any = {};

  STATUS_PUBLISHED = 10;
  STATUS_PENDING = 20;
  STATUS_DRAFT = 30;
  STATUS_WITHDRAWN = 40;

  private sub: Subscription;

  constructor(private productService: SellerProductService) {
    this.getStatistics();
    this.sub = this.productService.productListingUpdated.subscribe(() => this.getStatistics());
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  private getStatistics() {
    this.productService.fetchStatistics().subscribe(statistics => this.statistics = statistics);
  }

}
