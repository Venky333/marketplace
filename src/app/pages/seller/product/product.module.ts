import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {QuillModule} from 'ngx-quill';
import {NgbAlertModule, NgbModalModule, NgbTooltipModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {NgSelectModule} from '@ng-select/ng-select';
import {SharedModule} from 'app/shared.module';
import {ProductEditPageComponent} from './product-edit-page/product-edit-page.component';
import {ProductEditResolver} from './product-edit-page/product-resolver';
import {AngularCropperjsModule} from 'angular-cropperjs';
import {SellerProductService} from './serller-product.service';
import {SellerProductsPageComponent} from './seller-products-page.component';
import {ProductListingPageComponent} from './product-listing-page/product-listing-page.component';
import {ProductImageCropperComponent} from './product-edit-page/components/product-image-cropper.component';
import {ProductStatusLabelComponent} from './components/product-status-label/product-status-label.component';


@NgModule({
  imports: [
    SharedModule,
    NgSelectModule,
    NgbTooltipModule,
    NgbAlertModule,
    NgbModalModule,
    NgbTooltipModule,
    NgbTypeaheadModule,
    QuillModule,
    AngularCropperjsModule,
    RouterModule.forChild([
      {
        path: '',
        component: SellerProductsPageComponent,
        children: [
          {
            path: '',
            component: ProductListingPageComponent,
          },
          {
            path: 'edit/:name/:id',
            component: ProductEditPageComponent,
            resolve: {results: ProductEditResolver},
          },
          {
            path: 'new',
            component: ProductEditPageComponent,
            resolve: {results: ProductEditResolver},
          }
        ]
      },

    ])
  ],
  providers: [
    SellerProductService, ProductEditResolver
  ],
  declarations: [
    SellerProductsPageComponent,
    ProductListingPageComponent,
    ProductEditPageComponent,
    ProductImageCropperComponent,
    ProductStatusLabelComponent
  ],
  entryComponents: [
    ProductImageCropperComponent
  ]
})
export class ProductModule {

}
