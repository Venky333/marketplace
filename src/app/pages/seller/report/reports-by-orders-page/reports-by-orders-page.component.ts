import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {SubOrder} from 'app/models/sub-order';
import {RegionService} from 'app/services/region.service';
import {SellerOrderService} from '../../order/seller-order.service';
import {OrderDetailsModalComponent} from '../../../../components/order-details-modal/order-details-modal.component';

@Component({
  templateUrl: './reports-by-orders-page.component.html',
  styleUrls: ['./reports-by-orders-page.component.scss']
})
export class ReportsByOrdersPageComponent implements OnInit, OnDestroy {
  static TAG = 'ReportsByOrdersPageComponent';
  queryCall: Subscription;
  page = 1;
  pageSize = 10;
  sortBy = 'created_at';
  sortOrder = 'desc';
  hasError: boolean;
  totalCount: number;
  pageCount: number;
  modelsName = 'subOrders';
  models: Array<SubOrder> = [];
  showLoadMoreButton: boolean;
  state = SellerOrderService.STATE_SHIPPED;
  private subs: Array<Subscription>;

  constructor(private orderService: SellerOrderService,
              private router: Router,
              private regionService: RegionService,
              private modalService: NgbModal,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.subs = [
      this.route.queryParams.subscribe((params: Params) => {
        this.search(Number(params.page || 1));
      }),
    ];
  }

  showOrderDetail(order) {
    const modalRef = this.modalService.open(OrderDetailsModalComponent, {windowClass: 'modal-right'});
    modalRef.componentInstance.setOrder(order);
    modalRef.result
      .then(() => this.search())
      .catch(() => this.search());
  }


  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }
  }

  trackByOrderId(index, item) {
    if (!item) {
      return null;
    }
    return item.id;
  }

  toggleSort(property) {
    if (!property) {
      return;
    }

    if (this.sortBy === property) {
      switch (this.sortOrder) {
        case 'asc':
          this.sortOrder = 'desc';
          break;
        case 'desc':
          this.sortOrder = '';
          this.sortBy = '';
          break;
        default:
          this.sortOrder = 'asc';
      }
    } else {
      this.sortOrder = 'asc';
      this.sortBy = property;
    }

    this.search();
  }

  getNextPage() {
    this.showLoadMoreButton = false;
    this.search(this.page + 1, true);
  }

  private search(page: number = 1, loadMore = false) {
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }

    const queryParams = this.getQueryParams(page);
    console.debug(ReportsByOrdersPageComponent.TAG, 'search', queryParams);

    this.queryCall = this.orderService.query(queryParams).subscribe((results: any) => {
      this.page = page;
      const data = this.prepare(results[this.modelsName]);
      this.models = (loadMore) ? this.models.concat(data) : data;
      this.totalCount = results.meta.total;
      this.pageCount = results.meta.last_page;
      this.hasError = false;
      this.showLoadMoreButton = page < this.pageCount;
    }, () => {
      this.hasError = true;
      this.models = [];
    });
  }

  private getQueryParams(pageNumber) {
    const q: any = {
      'per-page': this.pageSize,
      page: pageNumber,
      expand: 'mainOrder,orderProducts,orderProducts.products'
    };
    if (this.sortBy && this.sortOrder) {
      q.sort = (this.sortOrder === 'asc' ? '' : '-') + this.sortBy;
    }
    if (this.state) {
      q.state = this.state;
    }
    return q;
  }

  private prepare(result) {
    return result.map(order => {
      order.shipping_country$ = this.getCountry(order);
      return order;
    })
  }

  private getCountry(order) {
    return this.regionService.getCountry(order.mainOrder.shipping_country_id).pipe(map(c => c.name));
  }
}
