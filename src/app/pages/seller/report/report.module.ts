import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';

import {SharedModule} from 'app/shared.module';
import {ReportsPageComponent} from './reports-page.component';
import {ReportsByOrdersPageComponent} from './reports-by-orders-page/reports-by-orders-page.component';

@NgModule({
  declarations: [
    ReportsPageComponent,
    ReportsByOrdersPageComponent
  ],
  imports: [
    SharedModule,
    NgbTooltipModule,
    NgbDropdownModule,
    RouterModule.forChild([
      {
        path: '',
        component: ReportsPageComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'by-orders'
          },
          {
            path: 'by-orders',
            component: ReportsByOrdersPageComponent,
          }
        ]
      }
    ])
  ],
})
export class ReportModule {

}
