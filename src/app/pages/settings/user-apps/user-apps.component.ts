import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '../../../../environments/environment';
import {AccountService} from 'app/services/account/account.service';

@Component({
  selector: 'app-user-apps',
  templateUrl: './user-apps.component.html',
  styleUrls: ['./user-apps.component.scss']
})
export class UserAppsComponent {

  apps$: Observable<any>;

  constructor(private accountService: AccountService) {
    this.apps$ = this.accountService.getAuthorizedApps().pipe(
      map(apps => apps.filter(app => app.id !== environment.oauthClientId))
    );
  }
}
