import {Component} from '@angular/core';
import {AccountService} from 'app/services/account/account.service';
import {ProfileType} from 'app/models/profile-type';


@Component({
  selector: 'app-edit-profile-page',
  templateUrl: './edit-profile-page.component.html'
})
export class EditProfilePageComponent {
  profile: any;
  TYPE_INDIVIDUAL = ProfileType.INDIVIDUAL;
  TYPE_CORPORATE = ProfileType.CORPORATE;

  constructor(private accountService: AccountService) {
    this.accountService.fetch().then(profile => this.profile = profile);
  }
}
