import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {catchError, concatMap, finalize, last, tap} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {Observable, of, throwError} from 'rxjs';

import {FormHelper} from 'app/helpers/utils';
import {AccountService} from 'app/services/account/account.service';

@Component({
  selector: 'app-corporate-profile-form',
  templateUrl: './corporate-profile-form.component.html',
})
export class CorporateProfileFormComponent implements OnChanges {
  ngForm: FormGroup;
  @Input() profile: any;
  saving = false;
  corporateTypes$: Observable<any>;
  industries$: Observable<any>;
  private logo: File;

  constructor(private formBuilder: FormBuilder,
              private accountService: AccountService,
              private toastr: ToastrService) {
    this.createForm();
    this.corporateTypes$ = this.accountService.fetchCorporateType();
    this.industries$ = this.accountService.fetchIndustries();
  }

  createForm() {
    this.ngForm = this.formBuilder.group(
      {
        name: [null, Validators.required],
        reg_number: [null, Validators.required],
        industry_id: [null, Validators.required],
        type_id: [null, Validators.required],
        phone: [null],
        fax_number: [null],
        contact_email: [null, [Validators.email]],
        address: [null],
        address_2: [null],
        country_id: [null],
        city: [null],
        state: [null],
        postcode: [null]
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('profile') && this.profile) {
      this.ngForm.patchValue({
        name: this.profile.corporateProfile.name,
        reg_number: this.profile.corporateProfile.reg_number,
        industry_id: this.profile.corporateProfile.industry_id,
        type_id: this.profile.corporateProfile.type_id,
        phone: this.profile.phone,
        fax_number: this.profile.corporateProfile.fax_number,
        contact_email: this.profile.corporateProfile.contact_email,
        address: this.profile.address,
        address_2: this.profile.address_2,
        country_id: this.profile.country_id,
        city: this.profile.city,
        state: this.profile.state,
        postcode: this.profile.postcode,
      });
    }
  }

  handleLogoSelect(file) {
    this.logo = file;
  }

  save() {
    if (!this.ngForm.valid) {
      FormHelper.markAsTouched(this.ngForm);
      return false;
    }
    this.saving = true;

    const upload = this.logo
      ? this.accountService.uploadImage(this.logo, 'logo').pipe(
        tap(() => this.logo = null),
        catchError((error) => {
          this.toastr.error('Error occurred while uploading logo');
          return throwError(error);
        }))
      : of(null);


    this.accountService.update(this.ngForm.value).pipe(
      catchError((response: HttpErrorResponse) => {
        if (response.status === 422) {
          const errors = response.error.errors;
          errors.map(error => {
            if (this.ngForm.contains(error.field)) {
              this.ngForm.get(error.field).setErrors({server: error.message})
            }
          })
        } else {
          this.toastr.error('Unknown error occurred during updating account')
        }
        return throwError(response);
      }), concatMap(() => upload)
    ).pipe(
      finalize(() => this.saving = false),
      last(item => {
        this.toastr.success('Profile updated successfully');
        return item;
      }))
      .subscribe()
  }

}
