import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {Country} from 'app/models/country';
import {State} from 'app/models/state';
import {RegionService} from 'app/services/region.service';

@Component({
  selector: 'app-profile-address-form',
  templateUrl: './profile-address-form.component.html'
})
export class ProfileAddressFormComponent implements OnInit, AfterViewInit {
  @Input() ngForm: FormGroup;
  countries: Country[];
  states: State[];
  stateLoading: boolean;
  countryLoading = true;

  constructor(private regionService: RegionService) {
    this.regionService.queryCountries().subscribe(countries => {
      this.countries = countries;
      this.countryLoading = false;
    });
  }

  ngOnInit(): void {
    this.ngForm.get('country_id').valueChanges.subscribe(value => {
      this.ngForm.get('state').setValue(null);
      this.fetchStates(value);
    });
  }

  ngAfterViewInit(): void {
    if (this.ngForm.value.country_id) {
      setTimeout(() => this.fetchStates(this.ngForm.value.country_id));
    }
  }

  private fetchStates(country_id) {
    this.states = [];
    if (country_id) {
      this.stateLoading = true;
      this.regionService.queryStates(country_id).subscribe(states => {
        this.states = states;
        this.stateLoading = false;
      });
    }
  }

}
