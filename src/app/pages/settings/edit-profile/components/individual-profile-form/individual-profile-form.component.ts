import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormHelper} from 'app/helpers/utils';
import {AccountService} from 'app/services/account/account.service';
import {ToastrService} from 'ngx-toastr';
import {catchError, concatMap, finalize, last, tap} from 'rxjs/operators';
import {of, throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-individual-profile-form',
  templateUrl: './individual-profile-form.component.html',
  styleUrls: ['individual-profile-form.component.scss']
})
export class IndividualProfileFormComponent implements OnChanges {
  ngForm: FormGroup;
  @Input() profile: any;
  saving = false;
  private avatar: File;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private toastr: ToastrService) {
    this.createForm();
  }

  createForm() {
    this.ngForm = this.formBuilder.group(
      {
        first_name: [null, Validators.required],
        last_name: [null, Validators.required],
        email: [null, [Validators.required, Validators.email]],
        mobile: [null, Validators.required],
        gender: [null],
        address: [null],
        address_2: [null],
        country_id: [null],
        city: [null],
        state: [null],
        postcode: [null]
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('profile') && this.profile) {
      this.ngForm.patchValue({
        first_name: this.profile.individualProfile.first_name,
        last_name: this.profile.individualProfile.last_name,
        email: this.profile.user.email,
        mobile: this.profile.mobile,
        gender: this.profile.individualProfile.gender,
        address: this.profile.address,
        address_2: this.profile.address_2,
        country_id: this.profile.country_id,
        city: this.profile.city,
        state: this.profile.state,
        postcode: this.profile.postcode,
      });
    }
  }

  handleAvatarSelect(file) {
    this.avatar = file;
  }

  save() {
    if (!this.ngForm.valid) {
      FormHelper.markAsTouched(this.ngForm);
      return false;
    }
    this.saving = true;

    const upload = this.avatar
      ? this.accountService.uploadImage(this.avatar, 'avatar').pipe(
        tap(() => this.avatar = null),
        catchError((error) => {
          this.toastr.error('Error occurred while uploading avatar');
          return throwError(error);
        }))
      : of(null);

    const value = this.ngForm.value;
    value.public_identity = `${value.first_name} ${value.last_name}`;
    this.accountService.update(value).pipe(
      catchError((response: HttpErrorResponse) => {
        if (response.status === 422) {
          const errors = response.error.errors;
          errors.map(error => {
            if (this.ngForm.contains(error.field)) {
              this.ngForm.get(error.field).setErrors({server: error.message})
            }
          })
        } else {
          this.toastr.error('Unknown error occurred during updating account')
        }
        return throwError(response);
      }), concatMap(() => upload)
    ).pipe(
      finalize(() => this.saving = false),
      last(item => {
        this.toastr.success('Profile updated');
        return item;
      }))
      .subscribe()
  }

}
