import {HttpErrorResponse} from '@angular/common/http';
import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

import {FormHelper} from 'app/helpers/utils';
import {AccountService} from '../../../services/account/account.service';


@Component({
  selector: 'app-addresses-settings',
  templateUrl: './addresses-settings.component.html',
  styleUrls: ['./addresses-settings.component.scss']
})
export class AddressesSettingsComponent {
  ngForm: FormGroup;
  loading = false;
  private profile: any;

  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private accountService: AccountService) {
    this.createForm();
    this.loadData();
  }

  copyFromProfile(to) {
    this.ngForm.patchValue({[to]: this.getAddressFromProfile()});
    this.ngForm.get(to).updateValueAndValidity({onlySelf: false});
    FormHelper.markAsTouched(this.ngForm.get(to));
  }

  update() {
    if (this.ngForm.invalid) {
      FormHelper.markAsTouched(this.ngForm);
      return;
    }

    this.loading = true;
    this.accountService.updateAddress(this.ngForm.value)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe(
        () => this.toastr.success('Address updated successfully'),
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 422) {
            errorResponse.error.errors.forEach((e) => {
              if (this.ngForm.get(e.field)) {
                this.ngForm.get(e.field).setErrors({server: e.message});
              }
            });
          } else {
            this.toastr.error('Error occurred while trying to update address: ' + errorResponse.message)
          }
        }
      )
  }

  private createForm() {
    const formConfig = {
      name: ['', Validators.required],
      phone: ['', [Validators.required]],
      address_1: ['', Validators.required],
      address_2: [''],
      country_id: ['', Validators.required],
      city: ['', Validators.required],
      state: [''],
      postcode: ['', Validators.required]
    };

    this.ngForm = this.formBuilder.group({
      billing: this.formBuilder.group(formConfig),
      shipping: this.formBuilder.group(formConfig),
    });
  }

  private loadData() {
    this.loading = true;
    Promise.all([
      this.accountService.fetchAddress().toPromise().then(address => this.ngForm.patchValue({
        billing: address.billing || {},
        shipping: address.shipping || {},
      })),
      this.accountService.fetch().then(profile => this.profile = profile)
    ]).then(() => this.loading = false)
  }

  private getAddressFromProfile(): any {
    return {
      name: this.profile.public_identity,
      phone: this.profile.phone,
      address_1: this.profile.address,
      address_2: this.profile.address_2,
      country_id: this.profile.country_id,
      city: this.profile.city,
      state: this.profile.state,
      postcode: this.profile.postcode,
    };
  }
}
