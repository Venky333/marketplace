import {FormBuilder, FormGroup} from '@angular/forms';
import {AfterViewInit, Component, Input} from '@angular/core';
import {Observable, of} from 'rxjs';
import {RegionService} from '../../../../services/region.service';

@Component({
  selector: 'app-user-address',
  templateUrl: './user-address-form.component.html',
  styleUrls: ['./user-address-form.component.scss']
})
export class UserAddressFormComponent implements AfterViewInit {
  @Input() form: FormGroup;
  countries$: Observable<any>;
  states$: Observable<any>;

  constructor(private formBuilder: FormBuilder, private regionService: RegionService) {
    this.countries$ = this.regionService.queryCountries();
    this.states$ = of([]);
  }

  ngAfterViewInit(): void {

    this.form.get('country_id').valueChanges.subscribe((value) => {
      this.form.get('state').setValue(null);
      this.fetchStates(value)
    });
  }

  private fetchStates(country_id) {
    this.states$ = country_id ? this.regionService.queryStates(country_id) : of([]);
  }
}
