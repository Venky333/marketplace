import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {NgSelectModule} from '@ng-select/ng-select';

import {SharedModule} from 'app/shared.module';
import {BuyerMenubarComponent} from 'app/components/buyer-menu-bar/buyer-menubar.component';
import {BuyerMobileMenuComponent} from 'app/components/buyer-mobile-menu/buyer-mobile-menu.component';
import {IndividualProfileFormComponent} from './edit-profile/components/individual-profile-form/individual-profile-form.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {AddressesSettingsComponent} from './addresses-settings/adresses.component';
import {SettingsComponent} from './settings.component';
import {UserAppsComponent} from './user-apps/user-apps.component';
import {EditProfilePageComponent} from './edit-profile/edit-profile-page.component';
import {ProfileAddressFormComponent} from './edit-profile/components/profile-address-form/profile-address-form.component';
import {CorporateProfileFormComponent} from './edit-profile/components/corporate-profile-form/corporate-profile-form.component';
import {UserAddressFormComponent} from './addresses-settings/user-address-form/user-address-form.component';

@NgModule({
  imports: [
    SharedModule,
    NgSelectModule,
    RouterModule.forChild([
      {
        path: '',
        component: SettingsComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'profile'
          },
          {
            path: 'change-password',
            component: ChangePasswordComponent
          },
          {
            path: 'addresses',
            component: AddressesSettingsComponent
          },
          {
            path: 'profile',
            component: EditProfilePageComponent
          },
          {
            path: 'auth-apps',
            component: UserAppsComponent
          }
        ]
      },
      {
        path: '',
        outlet: 'menubar',
        component: BuyerMenubarComponent,
      }
      , {
        path: '',
        outlet: 'mobile-menu',
        component: BuyerMobileMenuComponent,
      }
    ])
  ],
  declarations: [
    AddressesSettingsComponent,
    UserAppsComponent,
    ChangePasswordComponent,
    EditProfilePageComponent,
    IndividualProfileFormComponent,
    CorporateProfileFormComponent,
    ProfileAddressFormComponent,
    SettingsComponent,
    UserAddressFormComponent
  ]
})
export class SettingsModule {
}
