import {Component} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

import {FormHelper, matchingPasswords} from 'app/helpers/utils';
import {AccountService} from 'app/services/account/account.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent {
  form: FormGroup;
  loading: boolean;

  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private accountService: AccountService) {
    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      current_password: ['', Validators.required],
      password: ['', Validators.required],
      password_confirmation: ['', [Validators.required]]
    }, {validator: matchingPasswords('password', 'password_confirmation')});
  }

  changePassword() {
    if (this.form.invalid) {
      FormHelper.markAsTouched(this.form);
      return false;
    }

    this.loading = true;
    this.accountService.changePassword(this.form.value)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe(
        () => {
          this.toastr.success('Password changed successfully');
          this.form.reset();
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 422) {
            const errors = errorResponse.error.errors;
            errors.map(error => {
              if (this.form.contains(error.field)) {
                this.form.get(error.field).setErrors({server: error.message})
              }
            })
          } else {
            this.toastr.error('Error occurred while trying to change password: ' + errorResponse.message)
          }
        }
      )

  }
}
