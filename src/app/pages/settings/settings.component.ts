import {Component, EventEmitter, Inject, Input, OnDestroy, Output} from '@angular/core';
import {Subscription} from 'rxjs';

import {AUTH_SERVICE_CONFIG, AuthService} from 'app/services/auth';
import {ShoppingCartService} from 'app/services/shopping-cart/shopping-cart.service';
import {OauthUrls} from 'app/models/oauth-urls';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnDestroy {
  static TAG = 'SettingsComponent';

  registerUrl: string;
  logoutUrl: string;
  loginUrl: string;
  itemsCount: number;

  @Input() user: any;

  @Output() logout = new EventEmitter<string>();

  private subs: Array<Subscription>;

  constructor(@Inject(AUTH_SERVICE_CONFIG) public authConfig,
              private authService: AuthService,
              private shoppingCartService: ShoppingCartService) {

    this.itemsCount = this.shoppingCartService.getItemsCount();
    this.authService.getOauthUrl().subscribe((urls: OauthUrls) => {
      this.registerUrl = urls.registerUrl;
      this.logoutUrl = urls.logoutUrl;
      this.loginUrl = urls.loginUrl;
    });
    this.subs = [this.shoppingCartService.shoppingCartChangeBus.subscribe(() => this.itemsCount = this.shoppingCartService.getItemsCount())];
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  logoutUser() {
    this.logout.emit(this.logoutUrl);
  }

}
