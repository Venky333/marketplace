import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';

import {Order} from 'app/models/order';
import {OrderService} from 'app/services/order.service';
import {ProductService} from 'app/services/product';
import {ShoppingCartService} from 'app/services/shopping-cart';

@Component({
  templateUrl: './order-complete-page.component.html',
  styleUrls: ['./order-complete-page.component.scss']
})
export class OrderCompletePageComponent implements OnInit, OnDestroy {
  order$: Observable<Order>;
  hasError = false;
  orderNumber: string;
  private subs: Array<Subscription> = [];

  constructor(private route: ActivatedRoute,
              private productService: ProductService,
              private shoppingCartService: ShoppingCartService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    this.subs = [
      this.route.params.subscribe((params: any) => {
        this.order$ = this.orderService.fetch(params.order_id)
          .pipe(
            tap((order: any) => {
                this.orderNumber = order.number;

              }
            )
          )
      })

    ];
    this.shoppingCartService.setItems([]);
    this.orderService.storeLastCheckoutData(null)

  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
  }
}

