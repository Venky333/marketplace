import {Component} from '@angular/core';

@Component({
  selector: 'app-terms-page',
  templateUrl: './terms-page.component.html',
  styles: [
      `
      .container {
        max-width: 800px;
        text-align: justify;
      }
    `
  ]
})
export class TermsPageComponent {
}
