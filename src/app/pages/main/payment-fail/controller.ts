import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {OrderService} from '../../../services/order.service';

@Component({
  template: `
    <div class="">
      <div class="center-block w-auto-xs p-y-md ">
        <div class="p-a-md">
          <app-loading-placeholder></app-loading-placeholder>
        </div>
      </div>
    </div>`
})
export class PaymentFailPageComponent {
  constructor(private router: Router, private orderService: OrderService) {
    const data = this.orderService.getLastCheckoutData();
    this.router.navigateByUrl(data ? data.url : '/checkout');
  }
}
