import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

import {OrderStatusEnum} from 'app/models/order-status-enum';
import {SellerOrderService} from 'app/pages/seller/order/seller-order.service';
import {RegionService} from 'app/services/region.service';
import {ShippingService} from '../../../../services/shipping.service';


@Component({
  selector: 'app-order-info-footer',
  templateUrl: './order-info-footer.component.html',
  styleUrls: ['./order-info-footer.component.scss']
})
export class OrderInfoFooterComponent implements OnChanges {
  STATUS_COMPLETE = OrderStatusEnum.COMPLETED;
  @Input() order;
  shippingCountry$: Observable<string>;
  shippingServices = new Map<number, string>();

  constructor(private shippingService: ShippingService,
              private regionService: RegionService) {
    this.shippingService.fetch().subscribe(
      (resp: any) => resp.shipping_services.map(s => this.shippingServices.set(s.id, s.name))
    )
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('order') && this.order) {
      this.shippingCountry$ = this.regionService.getCountry(this.order.shipping_country_id)
        .pipe(
          map(c => c.name)
        )
    }
  }


}
