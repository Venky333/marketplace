import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {Observable, of, Subscription} from 'rxjs';

import {CartItem} from 'app/services/shopping-cart/models';
import {AuthService} from 'app/services/auth/service';
import {OrderService} from 'app/services/order.service';
import {ShoppingCartService} from 'app/services/shopping-cart/shopping-cart.service';
import {WindowRefService} from 'app/services/window';
import {RegionService} from 'app/services/region.service';
import {AccountService} from 'app/services/account/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-checkout-form',
  templateUrl: './checkout-form.component.html',
  styleUrls: ['./checkout-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckoutFormComponent implements OnInit, OnChanges, OnDestroy {

  form: FormGroup;
  countries$: Observable<any>;
  states$: Observable<any>;
  total = 0;
  isLoggedIn: boolean;

  @Input() hideEditCartBtn = false;
  @Input() items: Array<CartItem>;
  @ViewChild('ngPaymentForm') ngPaymentForm: NgForm;

  private window: Window;
  private subs: Array<Subscription> = [];

  constructor(private regionService: RegionService,
              private fb: FormBuilder,
              private accountService: AccountService,
              private authService: AuthService,
              private orderService: OrderService,
              private router: Router,
              private cd: ChangeDetectorRef,
              windowRef: WindowRefService) {
    this.window = windowRef.nativeWindow;
    this.countries$ = this.regionService.queryCountries();
    this.isLoggedIn = this.authService.isLoggedIn();
    this.createForm();
    this.subs = [this.authService.tokenChangeBus.subscribe(token => {
      if (token) {
        this.isLoggedIn = true;
        this.fetchAddress();
      } else {
        this.isLoggedIn = false;
      }
      this.checkGuestFields();
    })];
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.fetchAddress();
    }
    this.calcTotals();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.items) {
      this.form.setControl('items', this.createItems(changes.items.currentValue));
      this.calcTotals();
    }
  }

  createForm() {
    this.form = this.fb.group({
        first_name: ['', Validators.required],
        last_name: ['', Validators.required],
        company: [''],
        phone: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        address_1: ['', [Validators.required]],
        address_2: [''],
        city: ['', [Validators.required]],
        postcode: ['', [Validators.required]],
        country_id: ['', [Validators.required]],
        state: [''],
        comment: [''],
        items: this.createItems(this.items)
      }
    );

    this.form.get('country_id').valueChanges.subscribe((value) => {
      this.form.get('state').setValue(null);
      this.fetchStates(value)
    });

    this.checkGuestFields();
    const data = this.orderService.getLastCheckoutData();
    if (data) {
      this.form.patchValue(data.data);
      this.cd.markForCheck();
    }
  }


  proceedPayment() {
    if (this.form.valid) {
      this.orderService.checkout(this.form.value)
        .subscribe((resp: any) => {
          this.orderService.storeLastCheckoutData({data: this.form.value, url: this.router.url.toString()});
          this.window.location.href = resp.url;
        });
    }
  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
  }

  private calcTotals() {
    const {total} = ShoppingCartService.calculateTotals(this.items);
    this.total = total;
  }

  private fetchStates(country_id) {
    this.states$ = country_id ? this.regionService.queryStates(country_id) : of([]);
  }

  private createItems(items) {
    const itemsConfig = items ? items.map(item => this.fb.group({product_id: item.product_id, quantity: item.quantity})) : [];
    return this.fb.array(itemsConfig, Validators.required);
  }

  private fetchAddress() {
    if (this.orderService.getLastCheckoutData()) {
      return;
    }
    this.accountService.fetchAddress()
      .subscribe((address: any) => {
        this.form.patchValue(address.shipping || {});
        this.cd.markForCheck();
      });
  }


  private checkGuestFields() {
    if (this.isLoggedIn) {
      if (this.form.get('password')) {
        this.form.removeControl('password');
      }
    } else if (!this.form.get('password')) {
      this.form.addControl('password', new FormControl('', [Validators.required, Validators.minLength(8)]));
    }
  }
}
