import {AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';

import {ProductService} from 'app/services/product';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, AfterViewInit {

  searchTerm: '';
  LIMIT = 4;
  popularProducts$: Observable<any>;
  @ViewChild('searchInput') searchInput: ElementRef<HTMLElement>;
  @Output() closed = new EventEmitter();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private productService: ProductService) {

  }

  ngOnInit() {
    this.popularProducts$ = this.productService.listing({
      sort: '-view_count',
      'per-page': this.LIMIT,
    }).pipe(map(resp => resp.products));
  }

  ngAfterViewInit(): void {
    this.searchInput.nativeElement.focus();
  }

  search() {
    this.closed.emit();
    this.router.navigate(['/listing'], {queryParams: {searchTerm: this.searchTerm}})
  }
}
