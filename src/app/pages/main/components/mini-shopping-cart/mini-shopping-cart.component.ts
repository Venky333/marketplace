import {Component} from '@angular/core';

import {ShoppingCartService} from 'app/services/shopping-cart';
import {ShoppingCartChangeEvent, CartItem} from 'app/services/shopping-cart/models';

@Component({
  selector: 'app-mini-shopping-cart',
  templateUrl: './mini-shopping-cart.component.html',
  styleUrls: ['./mini-shopping-cart.component.scss']
})
export class MiniShoppingCartComponent {
  items: Array<CartItem>;
  total: number;
  itemsCount: number;

  constructor(private shoppingCartService: ShoppingCartService) {
    this.items = shoppingCartService.getItems();
    this.total = shoppingCartService.getTotal();
    this.itemsCount = shoppingCartService.getItemsCount();
    this.shoppingCartService.shoppingCartChangeBus.subscribe((event: ShoppingCartChangeEvent) => {
      this.items = event.items;
      this.total = shoppingCartService.getTotal();
      this.itemsCount = shoppingCartService.getItemsCount();
    })
  }
}
