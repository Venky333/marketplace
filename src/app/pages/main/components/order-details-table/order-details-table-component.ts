import {Component, Input} from '@angular/core';

import {CartItem} from 'app/services/shopping-cart/models';

@Component({
  selector: 'app-order-details-table',
  templateUrl: './order-details-table-component.html',
  styleUrls: ['./order-details-table-component.scss']
})
export class OrderDetailsTableComponent {
  @Input() items: CartItem;
  @Input() total: number;
}
