import {Component, ElementRef, Inject, OnDestroy, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {ActivatedRoute, NavigationStart, Router} from '@angular/router';
import {fromEvent, merge, Subject, Subscription} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {AUTH_SERVICE_CONFIG, AuthService} from 'app/services/auth';
import {WindowRefService} from 'app/services/window';
import {AuthToken, User} from 'app/models';
import {AccountService} from 'app/services/account/account.service';
import {Key} from 'app/helpers/key';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit, OnDestroy {

  static TAG = 'MainPageComponent';
  user: User;
  searchOpen = false;
  logoUrl: string;
  isSeller: boolean;
  private destroyed$ = new Subject();
  private searchClosed$ = new Subject();

  constructor(private route: ActivatedRoute,
              private router: Router,
              private elementRef: ElementRef,
              @Inject(AUTH_SERVICE_CONFIG) public authConfig,
              @Inject(DOCUMENT) private _document: any,
              private windowRef: WindowRefService,
              private authService: AuthService,
              private accountService: AccountService) {
    this.initTokenListener();
    this.logoUrl = environment.logoUrl;
  }

  ngOnInit() {
    this.route.data.forEach((data: any) => this.setUser(data.user));
    this.router.events
      .pipe(takeUntil(this.destroyed$))
      .subscribe((nav: any) => {
        if (nav instanceof NavigationStart) {
          this.closeSearch();
        }
      });

    this.accountService.personalizeChangeBus
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.setUser(this.accountService.getProfile()));
  }

  logout(url) {
    this.setUser(null);
    this.authService.logout();
    this.windowRef.nativeWindow.location.href = url;
  }

  openSearch() {
    this.searchOpen = true;
    setTimeout(() => {
      this.elementRef.nativeElement.scrollIntoView();
    }, 50);
    this.installSearchCloser();
  }

  closeSearch() {
    if (this.searchOpen) {
      this.searchOpen = false;
      this.searchClosed$.next();
    }
  }

  ngOnDestroy() {
    this.destroyed$.next();
  }


  private initTokenListener(): Subscription {
    return this.authService.tokenChangeBus
      .pipe(
        takeUntil(this.destroyed$)
      )
      .subscribe((t: AuthToken) => {
        if (t) {
          this.accountService.fetch(false).then(
            (user: User) => this.setUser(user),
            (fail) => console.error(MainPageComponent.TAG, 'userListener failed', fail)
          );
        } else {
          this.setUser(null);
        }
      });
  }

  private setUser(user: User) {
    this.user = user;
    if (this.user) {
      this.isSeller = this.authService.userCan(AuthService.MANAGE_PRODUCTS);
    }
  }

  private installSearchCloser() {
    fromEvent<KeyboardEvent>(this._document, 'keydown')
      .pipe(
        takeUntil(merge(this.destroyed$, this.searchClosed$)),
        // tslint:disable-next-line:deprecation
        filter(e => e.which === Key.Escape),
      ).subscribe(() => this.closeSearch());
  }
}
