import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';

import {RegionService} from 'app/services/region.service';
import {Order} from 'app/models/order';
import {ProductService} from 'app/services/product';
import {OrderService} from 'app/services/order.service';
import {OrderStatusEnum} from 'app/models/order-status-enum';

@Component({
  templateUrl: './order-details-page.component.html',
  styleUrls: ['./order-details-page.component.scss']
})
export class OrderDetailsPageComponent implements OnInit, OnDestroy {

  order$: Observable<Order>;
  hasError = false;
  orderNumber: string;
  STATUS_COMPLETE = OrderStatusEnum.COMPLETED;
  private subs: Array<Subscription> = [];

  constructor(private route: ActivatedRoute,
              private productService: ProductService,
              private regionService: RegionService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    this.subs = [
      this.route.params.subscribe((params: any) => {
        this.order$ = this.orderService.fetch(params.order_id)
          .pipe(
            tap((order: any) => {
                this.orderNumber = order.number;
              }
            )
          )
      })

    ];

  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
  }
}
