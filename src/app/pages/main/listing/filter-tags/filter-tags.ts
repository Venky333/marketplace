import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'filter-tags',
  templateUrl: './filter-tags.html',
  styleUrls: ['./filter-tags.scss']
})

export class FilterTags {

  @Input() filters: Array<any>; // object should have 'name' field for display

  @Output() removeFilter = new EventEmitter<any>();


}
