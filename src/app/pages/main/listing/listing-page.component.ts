import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router, UrlTree} from '@angular/router';
import {Subscription} from 'rxjs';

import {scrollToTop} from 'app/helpers/utils';
import {ProductService} from 'app/services/product';
import {CategoryService} from 'app/services/content/category.service';

@Component({
  selector: 'app-listing-page',
  templateUrl: './listing-page.component.html',
  styleUrls: ['./listing-page.component.scss']
})
export class ListingPageComponent implements OnDestroy, OnInit {
  static TAG = 'ListingPageComponent';

  queryCall: Subscription;
  sort = '-created_at';
  sortParamsDescription: Map<string, string> = new Map<string, string>();
  searchTerm = '';
  searchedTerm = '';
  page = 1;
  pageSize = 30;
  totalCount: number;
  pageCount: number;
  hasError = false;
  showLoadMoreButton = false;

  modelsName = 'products';
  models: Array<any> = [];
  relatedProductId: number;
  title = 'Products';
  tag: any;
  @ViewChild('listingBottom') compareHeader: ElementRef;

  private subs: Array<Subscription>;
  private urlTree: UrlTree;
  private is_special: string;
  private category: string;

  constructor(private productService: ProductService,
              private router: Router,
              private location: Location,
              private categoryService: CategoryService,
              private route: ActivatedRoute,
  ) {
    this.urlTree = router.parseUrl(router.url);
  }

  ngOnInit() {

    this.intiSortParams();
    this.subs = [
      this.route.queryParams.subscribe((params: any) => {
        this.searchTerm = params.searchTerm || '';
        this.is_special = params.is_special;
        this.tag = params.tag;
        this.category = params.category;
        this.initTitle();
        this.search(Number(params.page || 1));
      }),
    ];

  }

  intiSortParams() {
    this.sortParamsDescription.set('-published_at', 'Newest')
      .set('-view_count', 'Popularity')
      .set('name', 'Name A-Z')
      .set('price', 'Low to High')
      .set('-price', 'High to Low');


  }

  getNextPage() {
    this.showLoadMoreButton = false;
    this.search(this.page + 1, true);
  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }
  }

  private getQueryParams(pageNumber) {
    const q: any = {
      'per-page': this.pageSize,
      page: pageNumber
    };

    if (this.searchTerm && this.searchTerm.trim().length) {
      q.searchTerm = this.searchTerm.trim();
    }
    if (this.sort) {
      q.sort = this.sort;
    }
    if (this.category) {
      q.category = this.category;
    }
    if (this.tag) {
      q.tag_name__and = this.tag;
    }
    if (this.relatedProductId) {
      q.related = this.relatedProductId;
    }
    if (this.is_special) {
      q.special_and_discounted = 1;
    }
    return q;
  }

  private search(page: number = 1, loadMore = false) {

    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }

    const queryParams = this.getQueryParams(page);
    console.debug(ListingPageComponent.TAG, 'search', queryParams);

    this.queryCall = this.productService.listing(queryParams).subscribe((results: any) => {
      this.searchedTerm = queryParams.searchTerm || '';
      this.page = page;
      this.pageSize = queryParams.pageSize || this.pageSize;
      this.models = (loadMore) ? this.models.concat(results[this.modelsName]) : results[this.modelsName];
      this.totalCount = results.meta.total;
      this.pageCount = results.meta.last_page;
      this.showLoadMoreButton = page < this.pageCount;
      if (!loadMore) {
        scrollToTop();
      }
      this.hasError = false;
    }, () => {
      this.showLoadMoreButton = false;
      this.hasError = true;
      this.models = [];
    });
  }

  private initTitle() {
    if (this.is_special) {
      this.title = 'Deals';
    } else if (this.category) {
      this.categoryService.fetch(this.category).subscribe(c => {
        if (c && c.name) {
          this.title = c.name;
        }
      })
    } else {
      this.title = 'Products';
    }
  }
}
