import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {Product} from 'app/models';
import {CategoryService} from 'app/services/content';


@Component({
  selector: 'app-product-details-tab',
  templateUrl: './product-details-tab.component.html',
  styleUrls: ['./product-details-tab.component.scss']
})
export class ProductDetailsTabComponent implements OnChanges {
  @Input() product: Product;
  productCategoryName$: Observable<string>;
  hasDimension: boolean;
  hasShippingDimension: boolean;

  constructor(private  categoryService: CategoryService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('product') && this.product) {
      this.productCategoryName$ = this.categoryService.fetch(this.product.category_id).pipe(map(cat => cat.name));
      this.hasDimension = this.product.length !== null || this.product.width !== null || this.product.height !== null;
      this.hasShippingDimension = this.product.shipping_length !== null || this.product.shipping_width !== null
        || this.product.shipping_height !== null;
    }
  }

}
