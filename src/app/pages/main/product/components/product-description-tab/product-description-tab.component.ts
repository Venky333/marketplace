import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {Product, User} from 'app/models';
import {AccountService} from 'app/services/account/account.service';

@Component({
  selector: 'app-product-description-tab',
  templateUrl: './product-description-tab.component.html',
  styleUrls: ['./product-description-tab.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductDescriptionTabComponent implements OnChanges {

  @Input() product: Product;
  sellerFilter: any;
  sellerInfo$: Observable<User>;
  relatedFilter: any;

  constructor(private profileService: AccountService) {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('product') && this.product) {
      this.sellerFilter = {user_id: this.product.user_id, 'per-page': 4};
      this.relatedFilter = {'per-page': 4,related: this.product.id};
      this.sellerInfo$ = this.profileService.fetchProfile(this.product.user_id)
    }
  }


}
