import {Meta, Title} from '@angular/platform-browser';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Observable} from 'rxjs/internal/Observable';

import {ProductService} from 'app/services/product/product.service';
import {RecentlyViewedProductsService} from 'app/services/recently-viewed-products.service';
import {AccountService} from 'app/services/account/account.service';

import {Product} from 'app/models';
import {ErrorRerouteMixin} from 'app/helpers/error-reroute-mixin';

@Component({
  templateUrl: './product-details-page.component.html',
  styleUrls: ['./product-details-page.component.scss']
})
export class ProductDetailsPageComponent implements OnInit, OnDestroy {

  product: any;
  productId: number;
  sellerData$: Observable<any>;
  activeTab;
  TAB_INFO = 1;
  TAB_SPECS = 2;
  TAB_REVIEWS = 3;
  TAB_CONTACT = 4;
  private subs: Array<Subscription>;

  constructor(private productService: ProductService,
              private router: Router,
              private recentlyViewedService: RecentlyViewedProductsService,
              private route: ActivatedRoute,
              private titleService: Title,
              private metaService: Meta,
              private accountService: AccountService) {
  }

  ngOnInit() {
    this.subs = [
      this.route.params.subscribe((params: any) => {
        this.productId = params.id;
        this.product = null;
        this.productService.fetch(params.id, ['images', 'tags']).then((model: Product) => {
          if (!params.name || params.name !== model.link) {
            this.router.navigate([`/product/${model.link}/${model.id}`]);
          } else {
            this.activeTab = this.TAB_INFO;
            const title = this.titleService.getTitle().split(':')[0] + ': ' + model.name;
            this.titleService.setTitle(title);

            this.metaService.updateTag({property: 'og:title', content: title});
            this.product = model;

            this.fetchSellerData();
            this.recentlyViewedService.addProduct(this.product);
          }
        }, (fail) => ErrorRerouteMixin.handleErrorResp.call(this, fail));
      })
    ];

  }

  fetchSellerData() {
    this.sellerData$ = this.accountService.fetchProfile(this.product.user_id);
  }


  ngOnDestroy() {
    if (this.subs) {
      this.subs.forEach((s: Subscription) => s.unsubscribe());
    }
  }

  setTab(tab) {
    this.activeTab = tab;
  }
}
