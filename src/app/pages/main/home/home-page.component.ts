import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {ProductService} from 'app/services/product';
import {RecentlyViewedProductsService} from 'app/services/recently-viewed-products.service';
import {Product} from 'app/models';
import {map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})

export class HomePageComponent implements OnInit {
  popularProducts$: Observable<Array<Product>>;
  recentlyViewedProducts$: Observable<Array<Product>>;
  hasRecentlyViewed = false;
  specialProducts$: Observable<any>;

  LIMIT = 5;
  slides = [
    {
      image: 'assets/images/home/slide1.jpg',
      text: 'Handmade for the moments worth remembering.',
      link: null,
      linkLabel: 'Find something special',
    },
    {
      image: 'assets/images/home/slide2.jpg',
      text: 'In case your routine needs more pizzazz.',
      link: null,
      linkLabel: 'Shop handmade finds',
    },
    {
      image: 'assets/images/home/slide3.jpg',
      text: 'Customizable picks, as unique as the monster lovers in your life.',
      link: null,
      linkLabel: 'Explore now',
    },
  ];

  constructor(private productService: ProductService,
              private recentlyViewedService: RecentlyViewedProductsService) {
    this.hasRecentlyViewed = this.recentlyViewedService.recentlyViewedCount() >= 2;
  }

  ngOnInit() {
    this.recentlyViewedProducts$ = this.recentlyViewedService.getProducts(this.LIMIT)
    /*.pipe(
      mergeMap((products: Product[]) => {
        if (products.length === this.LIMIT) {
          return observableOf(products);
        } else if (products.length === 0) {
          return observableOf([]);
        }
        return this.productService.listing({
          category: products.map(p => p.category_id).join(','),
          id__not_in: products.map(p => p.id).join(','),
          'per-page': this.LIMIT - products.length,
          sort: '-view_count'
        }, false)
          .pipe(
            map((otherProducts: Product[]) => products.concat(otherProducts))
          );
      })
    )*/;
    this.specialProducts$ = this.productService.listing({sort: '-created_at', special_and_discounted: 1, 'per-page': this.LIMIT})
      .pipe(
        map(resp => resp.products),
        tap(resp => {
          const specialProductsIds = resp.map(p => p.id);
          this.popularProducts$ = this.productService.listing({
            sort: '-view_count',
            'per-page': this.LIMIT,
            id__not_in: specialProductsIds.join(',')
          }).pipe(map((res: any) => res.products));

        })
      );

  }


}
