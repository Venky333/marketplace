import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

import {ProductService} from 'app/services/product/product.service';
import {ErrorRerouteMixin} from 'app/helpers/error-reroute-mixin';
import {Product} from 'app/models';
import {CartItem} from 'app/services/shopping-cart/models';
import {ShoppingCartService} from 'app/services/shopping-cart/shopping-cart.service';


@Component({
  templateUrl: './buy-now-page.component.html',
  styles: [`
    app-quantity-input {
      display: block;
      width: 128px;
    }
  `]
})
export class BuyNowPageComponent implements OnInit, OnDestroy {

  product: any;
  quantity = 1;
  items: CartItem[];
  descriptionCollapsed = true;
  total = 0;

  private subs: Array<Subscription>;

  constructor(private service: ProductService,
              protected router: Router,
              private shoppingCartService: ShoppingCartService,
              protected route: ActivatedRoute) {
  }

  ngOnInit() {
    this.quantity = this.route.snapshot.queryParams['quantity'] ? parseInt(this.route.snapshot.queryParams['quantity'], 10) : 1;
    this.subs = [
      this.route.params.subscribe((params: any) => {
        this.service.fetch(params.id, ['images', 'primaryImage']).then((model: Product) => {
            if (model.status != ProductService.PRODUCT_STATUS_PUBLISHED) {
              this.router.navigate(['/not-found'], {replaceUrl: true});
              return;
            }
            this.items = [{quantity: this.quantity, product_id: model.id, product: model}];
            this.product = model;
            this.total = ShoppingCartService.calculateTotals(this.items).total;
          },
          (fail) => ErrorRerouteMixin.handleErrorResp.call(this, fail));
      })
    ];
  }

  updateQuantity(value) {
    this.quantity = value;
    this.items[0].quantity = value;
    this.items = this.items.slice();
    this.total = ShoppingCartService.calculateTotals(this.items).total;
  }

  continueShopping() {
    this.shoppingCartService.addItem(this.product, this.quantity).subscribe();
    this.router.navigateByUrl('/listing');
  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
  }

}
