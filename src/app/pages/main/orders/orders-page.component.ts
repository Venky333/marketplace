import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

import {OrderService} from 'app/services/order.service';
import {Order} from '../../../models/order';

@Component({
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss']
})
export class OrdersPageComponent implements OnDestroy {

  static TAG = 'OrdersPageComponent';

  queryCall: Subscription;
  sort = '-paid_at';
  page = 1;
  pageSize = 10;
  hasError: boolean;
  totalCount: number;
  pageCount: number;
  orders: Array<Order>;
  showLoadMoreButton = false;
  sortParamsDescription: Map<string, string> = new Map<string, string>();
  sortParamKeys: string[];

  constructor(private orderService: OrderService,
              protected router: Router,
              protected route: ActivatedRoute) {
    this.search();
    this.initSortParams()
  }

  getNextPage() {
    this.showLoadMoreButton = false;
    this.search(this.page + 1, true);
  }

  handleSort(sort) {
    if (this.sort !== sort) {
      this.sort = sort;
      this.search();
    }
  }

  ngOnDestroy() {
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }
  }

  private search(page: number = 1, loadMore = false) {
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }

    const queryParams = this.getQueryParams(page);
    console.debug(OrdersPageComponent.TAG, 'search', queryParams);

    this.queryCall = this.orderService.query(queryParams).subscribe((results: any) => {
      this.page = page;
      this.pageSize = queryParams.pageSize || this.pageSize;
      this.orders = (loadMore) ? this.orders.concat(results.orders) : results.orders;
      this.totalCount = results.meta.total;
      this.pageCount = results.meta.last_page;
      this.hasError = false;
      this.showLoadMoreButton = page < this.pageCount;
    }, () => {
      this.hasError = true;
      this.orders = [];
    });
  }

  private getQueryParams(pageNumber) {
    const q: any = {'per-page': this.pageSize, page: pageNumber, expand: 'orderProducts'};
    if (this.sort) {
      q.sort = this.sort;
    }
    return q;
  }

  private initSortParams() {
    this.sortParamsDescription
      .set('number', 'A-Z')
      .set('total', 'Lowest price')
      .set('-total', 'Highest price')
      .set('-paid_at', 'Latest ordered');

    this.sortParamKeys = Array.from(this.sortParamsDescription.keys());
  }

}
