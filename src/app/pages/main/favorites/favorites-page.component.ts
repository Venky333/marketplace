import {Component, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {ToastrService} from 'ngx-toastr';
import {FavoritesService} from 'app/services/favorites.service';
import {ShoppingCartService} from 'app/services/shopping-cart';
import {ProductModalViewComponent} from '../../../components/product-modal-view/product-modal-view.component';

@Component({
  selector: 'app-favorites-page',
  templateUrl: './favorites-page.component.html',
  styleUrls: ['./favorites-page.component.scss']
})
export class FavoritesPageComponent {
  sort = 'popular';
  page = 1;
  pageSize = 15;
  hasError: boolean;
  totalCount: number;
  pageCount: number;
  models: Array<any> = [];
  queryCall: Subscription;
  showLoadMoreButton = false;
  sortParamsDescription: Map<string, string> = new Map<string, string>();
  sortParamKeys: string[];
  loading = false;
  @ViewChild('confirmRemoveModal') confirmRemoveModal;
  private selected: any;

  constructor(private favoritesService: FavoritesService,
              private modalService: NgbModal,
              private shoppingCartService: ShoppingCartService,
              private toastr: ToastrService) {
    this.search();
    this.initSortParams();
  }

  getNextPage() {
    this.showLoadMoreButton = false;
    this.search(this.page + 1, true);
  }

  handleSort(sort) {
    if (this.sort !== sort) {
      this.sort = sort;
      this.search();
    }
  }

  confirmRemove(item) {
    this.selected = item;
    this.modalService.open(this.confirmRemoveModal, {windowClass: 'confirm-modal'})
      .result.catch(() => this.selected = null);
  }

  remove() {
    this.loading = true;
    this.favoritesService.remove(this.selected.id)
      .pipe(
        finalize(() => {
          this.loading = false;
          this.selected = null;
          this.search();
        })
      )
      .subscribe(
        () => this.toastr.success('Product removed from favourites'),
        fail => this.toastr.error(`Error occurred while trying remove favorite: ${fail.message}`)
      )
  }

  confirmBuy(item) {
    this.selected = item;
    const modalRef = this.modalService.open(ProductModalViewComponent, {
      centered: true,
      windowClass: 'modal--no-radius product-modal-view'
    });
    modalRef.componentInstance.product = item.product;
    modalRef.result.catch(() => this.selected = null);
  }

  private getQueryParams(pageNumber) {
    const q: any = {'per-page': this.pageSize, page: pageNumber};
    if (this.sort) {
      q.sort = this.sort;
    }
    return q;
  }

  private search(page: number = 1, loadMore = false) {
    if (this.queryCall) {
      this.queryCall.unsubscribe();
    }

    const queryParams = this.getQueryParams(page);

    this.queryCall = this.favoritesService.query(queryParams)
      .subscribe((results: any) => {
        this.page = page;
        this.pageSize = queryParams.pageSize || this.pageSize;
        this.models = (loadMore) ? this.models.concat(results['favorites']) : results['favorites'];
        this.totalCount = results.meta.total;
        this.pageCount = results.meta.last_page;
        this.showLoadMoreButton = page < this.pageCount;
        this.hasError = false;
      }, () => {
        this.hasError = true;
        this.models = [];
      });
  }

  private initSortParams() {
    this.sortParamsDescription
      .set('popular', 'Popular')
      .set('latest', 'Latest arrivals')
      .set('price', 'Lowest price')
      .set('-price', 'Highest price')
      .set('name', 'A-Z');

    this.sortParamKeys = Array.from(this.sortParamsDescription.keys());
  }
}
