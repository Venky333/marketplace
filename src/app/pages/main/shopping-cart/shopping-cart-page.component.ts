import {Component, OnDestroy, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {CartItem, ShoppingCartService} from 'app/services/shopping-cart';
import {FormHelper} from 'app/helpers/utils';
import {RegexUtil} from 'app/helpers/regex-util';
import {ToastrService} from 'ngx-toastr';
import {finalize} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart-page.component.html',
  styleUrls: ['./shopping-cart-page.component.scss']
})
export class ShoppingCartPageComponent implements OnDestroy {
  items: Array<CartItem>;
  total: number;
  loading: boolean;

  @ViewChild('confirmUpdateModal') confirmUpdateModal;
  @ViewChild('confirmContinueModal') confirmContinueModal;
  private subs: Array<Subscription>;
  private form: FormGroup;

  constructor(private shoppingCartService: ShoppingCartService,
              private fb: FormBuilder,
              private router: Router,
              private toastr: ToastrService,
              private modalService: NgbModal) {

    this.init();
    this.subs = [
      this.shoppingCartService.shoppingCartChangeBus.subscribe(() => this.init()),
    ];

  }

  removeItem(product_id, index) {
    this.items = this.items.filter(i => i.product_id !== product_id);
    this.form.markAsDirty();
    const control = this.form.get('items') as FormArray;
    control.removeAt(index);
  }

  checkout() {
    if (this.form.dirty) {
      this.modalService.open(this.confirmUpdateModal, {windowClass: 'confirm-modal'});
      return;
    }
    this.goCheckout();
  }

  updateAndCheckout() {
    this.update(() => this.goCheckout());
  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
  }

  update(done = null) {
    this.form.get('items').value.forEach((v, index) => {
      this.items[index].quantity = parseInt(v.quantity, 10);
    });
    this.loading = true;
    this.shoppingCartService.setItems(this.items)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe(
        () => {
          this.toastr.success('Cart updated successfully');
          if (done) {
            done();
          }
        },
        (fail) => this.toastr.error('Error occurred while trying update cart ' + fail.message)
      );
  }

  continue() {
    if (this.form.dirty) {
      this.modalService.open(this.confirmContinueModal, {windowClass: 'confirm-modal'});
      return;
    }
    this.goContinue();
  }

  updateAndContinue() {
    this.update(() => this.goContinue());
  }

  private goCheckout() {
    this.router.navigateByUrl('/cart/checkout');
  }

  private goContinue() {
    this.router.navigateByUrl('/home');
  }

  private init() {
    this.total = this.shoppingCartService.getTotal();
    const items = this.shoppingCartService.getItems();
    this.buildForm(items);
    this.items = items;
  }

  private buildForm(items) {
    this.form = this.fb.group({
      items: this.fb.array([])
    });
    const control = this.form.get('items') as FormArray;
    items.forEach(item => control.push(this.createItem(item)))
  }

  private createItem(item: CartItem) {
    return this.fb.group({
      quantity: [item.quantity, [Validators.required, FormHelper.biggerThan(0), Validators.pattern(RegexUtil.NUMBER_PATTERN)]]
    });
  }
}
