import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

import {ShoppingCartService} from 'app/services/shopping-cart/shopping-cart.service';
import {CartItem, ShoppingCartChangeEvent} from 'app/services/shopping-cart/models';

@Component({
  selector: 'app-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.scss']
})
export class CheckoutPageComponent implements OnInit, OnDestroy {
  items: Array<CartItem> = [];
  total: number;

  private subs: Array<Subscription> = [];

  constructor(private shoppingCartService: ShoppingCartService) {
  }

  ngOnInit(): void {
    this.items = this.shoppingCartService.getItems();
    this.total = this.shoppingCartService.getTotal();
    this.subs = [this.shoppingCartService.shoppingCartChangeBus.subscribe((event: ShoppingCartChangeEvent) => {
      this.items = event.items;
      this.total = this.shoppingCartService.getTotal();
    })];

  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
  }

}
