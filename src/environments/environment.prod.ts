export const environment = {
  production: true,
  communityId: 39,
  oauthClientId: 3,
  apiBaseUrl: '',
  communityApiBaseUrl: 'https://api.ukuya.com',
  accountApiBaseUrl: 'https://account.socoe.co/api',
  communityHomeUrl: 'http://e-wanita.socoe.co/',
  logoUrl: 'assets/images/epreneur-logo.png'
};
