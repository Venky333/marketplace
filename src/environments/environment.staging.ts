export const environment = {
  production: true,
  communityId: 39,
  oauthClientId: 3,
  apiBaseUrl: '',
  communityApiBaseUrl: 'https://api.ukuya.net',
  accountApiBaseUrl: 'https://account.ukuya.net/api',
  communityHomeUrl: 'http://e-wanita.socoe.co/',
  logoUrl: 'assets/images/epreneur-logo.png'
};
