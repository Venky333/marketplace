import { MarketplaceAngular2Page } from './app.po';

describe('marketplace-angular2 App', () => {
  let page: MarketplaceAngular2Page;

  beforeEach(() => {
    page = new MarketplaceAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
